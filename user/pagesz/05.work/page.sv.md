---
title: 'Jobba hos oss'
custom:
    content: "## Lediga tjänster / spontanansökan.\nJust nu har vi inga lediga tjänster, men skicka gärna en [spontanansökan](mailto:recruitment-jobtech@arbetsformedlingen.se) med en kort beskrivning av dig själv och vad du kan, så kontaktar vi dig när behov finns."
routes:
    default: /jobba-hos-oss
---

