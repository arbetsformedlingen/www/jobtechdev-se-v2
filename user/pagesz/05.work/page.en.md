---
title: 'Work with us'
custom:
    content: "## Vacancies / open application\nRight now we have no vacancies, but feel free to send a [open application](mailto:recruitment-jobtech@arbetsformedlingen.se) with a short description of yourself and what you can do, and we will contact you when needed."
routes:
    default: /work-with-us
---

