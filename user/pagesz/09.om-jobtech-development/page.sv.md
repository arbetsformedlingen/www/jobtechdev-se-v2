---
title: 'Om JobTech Development'
custom:
    content: "### Från ego till eko.\n## Arbetsmarknaden förändrar sig snabbare än någonsin och erbjuder hundratals digitala väglednings- och matchningstjänster. För att människor ska kunna navigera och hitta varandra behöver tjänsterna kommunicera och anpassas till nya behov. Det kräver en ny form av digital infrastruktur.\n\nJobTech Development är Arbetsförmedlingens satsning på en hållbar och gemensam infrastruktur för digitala matchningstjänster i Sverige. Den bygger på idén om färre silos och fler gemensamma tekniska strukturer, lösningar och standarder. Vi vill gå från ego till eko!\n\nPå Jobtech Development samlar vi matchningsaktörer i ett öppet nätverk för att samverka och dela data. Här finns öppna data, standarder och öppen källkod som är fritt tillgängligt för alla att använda. Genom vårt community finns möjlighet att diskutera, ställa frågor, visa upp jobb och utbyta idéer.\n\nJobTech Development vänder sig främst till människor och företag som utvecklar digitala tjänster för arbetsmarknaden. Vi samarbetar också med forskare och andra aktörer som vill ha fördjupad insikt om arbetsmarknaden med hjälp av öppna data.xxx\n<br><br><br><br>\n\n"
    menu:
        -
            title: 'samarbeta med oss'
            url: /sv/om-jobtech-development/samarbeta-med-oss
        -
            title: 'inspireras av andra'
            url: /sv/om-jobtech-development/inspireras-av-andra
        -
            title: 'Kontakta oss'
            url: /sv/om-jobtech-development/kontakta-oss
routes:
    default: /om-jobtech-development
---

