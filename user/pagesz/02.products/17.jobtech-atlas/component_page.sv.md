---
title: 'Jobtech Atlas'
custom:
    title: 'Jobtech Atlas'
    description: 'Ett gränssnitt som visar den data som finns i produkten Jobtech Taxonomy.'
    block_1: "Jobtech Atlas är en webbplats för användare att utforska databasen bakom Jobtech Taxonomy. Databasen innehåller strukturer, begrepp och relationer som används för att beskriva arbetsmarknaden.\nDär kan du söka bland listor av begrepp som ofta förekommer i matchning på arbetsmarknaden, exempelvis kompetensbegrepp, yrkesbenämningar med koppling till SSYK-strukturen (Standard för svensk yrkesklassificering) och sökbegrepp som kan användas för att underlätta vid sökning och filtrering.  Taxonomi-databasen förvaltas av Arbetsförmedlingen och bygger på information inhämtad från bland annat branschorganisationer, arbetsgivare och arbetssökande. Informationen finns även tillgänglig via Jobtech Taxonomy-API:et. \n\n## Vilket problem löser produkten?\nEn viktig del i arbetet med att producera och leverera data är att göra det lättillgängligt. Med Jobtech Atlas tillgängliggörs den information som finns i Jobtech Taxonomy i ett format som användare har enklare att läsa och förstå än exempelvis ett API. \n\n## För vem är produkten skapad?\nAlla ska kunna se och bidra till det språk som används på arbetsmarknaden. Dessa uppgifter har tidigare varit svåra att komma åt för icke-utvecklare eller personer utanför Arbetsförmedlingen. För att lösa problemet och göra informationen tillgänglig för alla har vi byggt Jobtech Atlas."
    block_2: "### Notera\nProjektet är under utveckling och bör inte ses som produktionsklart. Funktioner kan komma att läggas till, tas bort eller modifieras. Designen är inte finjusterad och kommer sannolikt att ändras / förbättras.\nFör tillfället har vi fokuserat på att göra kompetenser och yrkesdata tillgängliga. Resterande data kommer snart också att finnas tillgängliga.\n\nVi välkomnar all eventuell feedback."
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
    product_info:
        -
            title: Version
            value: Beta
    menu:
        -
            title: Användargränssnitt
            url: 'https://atlas.jobtechdev.se/page/taxonomy.html'
            showInShort: '1'
        -
            title: 'Teknisk dokumentation'
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas/-/blob/master/README.md'
            showInShort: '1'
taxonomy:
    category:
        - Gränssnitt
    type:
        - 'Öppna data'
    status:
        - Beta
---

