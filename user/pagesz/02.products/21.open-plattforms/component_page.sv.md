---
title: 'Open platforms'
custom:
    title: 'Open platforms'
    description: 'Ett försöksprojekt med syfte att underlätta för giggare att använda data som verifierar individens erfarenhet genererad från gig.'
    contact_email: lisa.hemph@arbetsformedlingen.se
    contact_name: 'Lisa Hemph'
    block_1: "Detta försöksprojekt utgår ifrån hypotesen om att vi tror att om vi underlättar för giggaren att använda data som verifierar hens erfarenhet genererad från gig öppnas det upp nya vägar och möjligheter till matchning och omställning. \n\nProblemet en digital infrastruktur för rörlighet av data i gigekonomin potentiellt kan lösa är delvis på systemnivå samt på individnivå. På individnivå är problemet att det idag finns en begränsad möjlighet för giggare att använda data som verifierar giggarens erfarenhet genererad från gig. Detta leder till hinder när giggaren söker efter arbete i olika kontexter eller former. På systemnivå är problemet kopplat till en risk för en ineffektiv marknad för matchning. Tillgången till verifierad erfarenhetsdata är idag en kostsam tröskel för nya aktörer till matchings marknaden vilket riskerar att leda till att matchningsaktörer inte når ut med sina erbjudanden på ett effektivt sätt. \n\nOpen platforms är ett försöksprojekt som har som syfte att utforska vilka effekter en digital infrastruktur som underlätta användandet av den data som bekräftar giggarens erfarenhet genererad från gig skulle ha för effektiviteten av matchningen och omställningstakten av arbete.\n\n- Vinsten för plattformar är att de kan erbjuda ett smidigare erbjudande till sina användare att komma igång med deras matchningserbjudande.\n\n- Vinsten för giggare är hen kan matchas effektivare och möjligheten att ställa om ökar."
    block_2: "### Mer information\nLäs mer på [www.openplatforms.org](https://www.openplatforms.org) om de pilotförsök vi arbetar med just nu."
    menu:
        -
            title: Webbplats
            url: 'https://www.openplatforms.org'
            showInShort: '1'
        -
            title: Demo
            url: 'https://www.mydigitalbackpack.org'
            showInShort: '1'
taxonomy:
    category:
        - Projekt
    type:
        - 'Öppna data'
    status:
        - Beta
---

