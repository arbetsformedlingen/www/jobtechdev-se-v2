---
title: 'Open platforms'
custom:
    title: 'Open platforms'
    description: 'A pilot project aimed at making it easier for gig workers to use data that verifies the individual''s experience generated from the gig.'
    contact_email: lisa.hemph@arbetsformedlingen.se
    contact_name: 'Lisa Hemph'
    block_1: "This pilot project is based on the hypothesis that we believe that if we make it easier for the gigger to use data that verifies her experience generated from the gig, new paths and opportunities for matching and adjustment will open up.\n\nThe problem a digital infrastructure for the mobility of data in the gig economy can potentially solve is partly at the system level as well as at the individual level. At the individual level, the problem is that today there is a limited opportunity for giggers to use data that verifies the gigger's experience generated from the gig. This leads to obstacles when the gigger is looking for work in different contexts or forms. At the system level, the problem is linked to a risk of an inefficient matching market. The availability of verified experience data is today a costly threshold for new players in the matching market, which risks leading to matching players not reaching out with their offers in an efficient manner.\n\nOpen platforms is a pilot project that aims to explore the effects a digital infrastructure that facilitates the use of the data that confirms the gig's experience generated from the gig would have on the efficiency of the matching and the pace of adaptation of work.\n\n- The benefit for platforms is that they can offer a smoother offer to their users to get started with their matching offer.\n\n- The profit for gigs is that it can be matched more efficiently and the possibility of adjusting increases."
    block_2: "### More information\nRead more at [www.openplatforms.org](https://www.openplatforms.org) about the pilot trials we are currently working on."
    menu:
        -
            title: Website
            url: 'https://www.openplatforms.org/en/home'
            showInShort: '1'
        -
            title: Demo
            url: 'https://www.mydigitalbackpack.org/en/home'
            showInShort: '1'
taxonomy:
    category:
        - Project
    type:
        - 'Open data'
    status:
        - Beta
---

