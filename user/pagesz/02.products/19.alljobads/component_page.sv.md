---
title: AllJobAds
custom:
    title: AllJobAds
    description: 'En widget som hämtar och visar utvalda jobb från Arbetsförmedlingens plattform Platsbanken.'
    block_1: "AllJobAds är en plug-and-play-widget som hämtar och visar jobb från Arbetsförmedlingens plattform Platsbanken. Widgeten kan ställas in för definierade områden och möjlighet finns att filtrera resultat.\n\n## Vilket problem löser produkten?\nMånga företag och organisationer önskar att få en en enkel lista med tillgängliga jobb på sin webbplats. Widgeten är utformad för att lätt kunna ändras och skräddarsys utifrån specifika önskemål och skapa mervärde för webbplatsens besökare.\n\n## För vem är produkten skapad?\nAllJobAds är huvudsakligen avsedd för företag och organisationer som vill guida personer till jobb inom ett specifikt område, t.ex. kommun, företag eller yrke."
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
    menu:
        -
            title: Demo
            url: 'https://widgets.jobtechdev.se/alljobads/'
            showInShort: '1'
        -
            title: 'Demo (utan modal)'
            url: 'https://widgets.jobtechdev.se/alljobads/notModal.html'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://github.com/MagnumOpuses/allJobAdsWidget'
            showInShort: '1'
        -
            title: README
            url: 'https://github.com/MagnumOpuses/allJobAdsWidget/blob/master/README.md'
            showInShort: null
taxonomy:
    category:
        - Widget
    type:
        - 'Öppen källkod'
---

