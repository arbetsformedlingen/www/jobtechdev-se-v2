---
title: 'Connect Once'
custom:
    title: 'Connect Once'
    description: 'Transfer CV data stored at Arbetsförmedlingen to external systems with the individual''s consent.'
    block_1: "Connect Once is a data transfer service that sends structured data between two systems. The service makes it possible for a person to automatically fill in a job application with the help of the CV information stored at Arbetsförmedlingen.\n\n## What problem does the product solve?\nRecruiters and employers experience that jobseekers drop out or do not complete applications due to the complexity and length of the application form. Connect Once simplifies the job application by allowing jobseekers to use their CV data stored at Arbetsförmedlingen to fill in their form with a simple process. By shortening the time required to apply for a job, the volume of completed applications to the employer will increase and the jobseeker's experience of the process will improve.\n\n## For whom is the product created?\nConnect Once is intended for recruiters and employers who want to increase the proportion of completed applications. The product can also be used to send other structured data between two systems such as registration form for online services."
    block_2: "### Connect Once API\nThe API is currently being beta tested and will be available to everyone shortly. Contact us for more information or if you wish to test the service."
    product_info:
        -
            title: License
            value: 'Apache License 2.0'
        -
            title: Version
            value: 1.1.0-beta
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berntson'
    menu:
        -
            title: Demo
            url: 'https://github.com/MagnumOpuses/af-connect-demo/blob/master/README.md'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://github.com/MagnumOpuses/af-connect/blob/master/README.md'
            showInShort: '1'
taxonomy:
    category:
        - API
    type:
        - 'Open data'
        - 'Open source'
    status:
        - Beta
---

