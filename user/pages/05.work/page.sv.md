---
title: 'Jobba hos oss'
custom:
    content: "Just nu har vi inga lediga tjänster, men skicka gärna en [spontanansökan](mailto:jobtechrecruitment@arbetsformedlingen.se) med en kort beskrivning av dig själv och vad du kan, så kontaktar vi dig när behov finns.\n\n \n"
    title: 'Lediga tjänster / Spontanansökan'
    short: 'Lediga tjänster / Spontanansökan'
routes:
    default: /jobba-hos-oss
---

