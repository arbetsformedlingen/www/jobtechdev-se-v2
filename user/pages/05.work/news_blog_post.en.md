---
title: 'Work with us'
custom:
    content: "<!--We are looking for talents and developers! We are happy to welcome you as our new colleague. Are you the one we are looking for?-->\n\nAt the moment, we have no vacancies, but feel free to [send us](mailto:jobtechrecruitment@arbetsformedlingen.se) your open application with a short description of yourself, your competences and skills and how you would like to contribute to what we do for digitalizing the future labour market in Sweden. We will get back to you if needed. \n\nIn the meanwhile, check out regularly our [Job Board (Platsbanken)](https://arbetsformedlingen.se/platsbanken/), if there are any open vacancies.\n\n\n\nLooking forward to hearing from you! "
    title: 'Vacancy List / Open Application '
    short: 'Vacancy List / Open Application '
routes:
    default: /work-with-us
---

