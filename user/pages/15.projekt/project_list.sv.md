---
title: projekt
custom:
    title: Utvecklingsprojekt
    text: 'Här hittar du de utvecklingsprojekt som plattformen driver just nu'
    showLeftMenu: false
    projects:
        -
            page: /projekt/individdata-och-dataportabilitet
        -
            page: /projekt/taxonomi-och-begreppsstruktur
        -
            page: /projekt/test
        -
            page: /projekt/digital-yrkesvaegledning
        -
            page: /projekt/test2
---

