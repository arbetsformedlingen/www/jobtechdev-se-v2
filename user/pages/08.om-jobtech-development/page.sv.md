---
title: 'Om JobTech Development'
custom:
    content: "Dagens arbetsmarknad erbjuder hundratals digitala matchnings- och vägledningstjänster. En fragmenterad arbetsmarknad med så många tjänster är svår att navigera i för såväl arbetssökande som arbetsgivare. \n\nJobTech Development är Arbetsförmedlingens satsning på en hållbar och gemensam infrastruktur för digitala matchningstjänster i Sverige. Den bygger på idén om färre silos och fler gemensamma tekniska lösningar och standarder. I samarbete med andra vill vi bidra till innovation och hållbara lösningar som är enkla att använda för alla. \nVi vill gå från ego till eko! \n\nHos Jobtech Development samlas aktörer från privat och offentlig sektor i ett öppet digitalt ekosystem för att samverka och dela data. Ekosystemet har i dag ca 200 företag och organisationer som aktivt delar data, kunskap och kod på vår plattform – fritt tillgängligt för alla att använda. \nVi samarbetar också med forskare och andra aktörer som vill ha fördjupad insikt om arbetsmarknaden med hjälp av öppna data. \n\nGenom att samverka och dela data skapar vi tillsammans en bättre arbetsmarknad för alla!"
    menu:
        -
            title: 'Samarbeta med oss'
            url: /sv/om-jobtech-development/kom-igang
        -
            title: 'Inspireras av andra'
            url: /sv/om-jobtech-development/inspireras-av-andra
        -
            title: 'Kontakta oss'
            url: /sv/om-jobtech-development/kontakta-oss
    title: 'Om JobTech Development'
    ingress: 'En bättre arbetsmarknad genom samarbete och öppna data'
routes:
    default: /om-jobtech-development
---

