---
title: 'Contact us'
custom:
    menu:
        -
            title: 'Collaborate with us'
            url: /en/about-jobtech-development/kom-igang
        -
            title: 'Get inspired by others'
            url: /en/about-jobtech-development/inspireras-av-andra
        -
            title: 'Contact us'
            url: /en/about-jobtech-development/kontakta-oss
    content: "If you want to get any help or support related to our components (projects, APIs, datasets, products, etc), we encourage you to drop a line on our [community forum](https://forum.jobtechdev.se), when our developers and/or users can get back to you. If you prefer using a mail correspondance, send us your questions [via mail](mailto:jobtechdev@arbetsformedlingen.se).\n\nIf you are interested in working with us, we will post all our ads either [here](https://jobtechdev.se/en/work-with-us), or the Swedish Public Employment Service's Job Board [Platsbanken](https://arbetsformedlingen.se/platsbanken). We also accept open vacancies [via mail](mailto:jobtechrecruitment@arbetsformedlingen.se ).\n\nIf you have questions regarding cooperation or would like to contribute in any way to the digital ecosystem, feel free to email our [community team](mailto:jobtechdev@arbetsformedlingen.se) for further information and assistance.\n\nIf you would like to meet us, (most of us are working remotely), but we will be happy to meet you at our office in central [Stockholm](https://www.google.com/maps/place/Slottsbacken+4,+111+30+Stockholm/@59.3258895,18.0706078,17z/data=!3m1!4b1!4m5!3m4!1s0x465f9d587818d6bb:0x6a3563e7fe09a3bd!8m2!3d59.3258895!4d18.0727965) and in Visby (Gotland). \n \nLooking forward to meeting you!  \n  \n<BR><BR><BR><BR>"
    title: 'Contact us'
    ingress: 'We are happy that you want to get in touch with us. We try to get back to you as soon as possible, providing you with answers to your questions, thoughts or feedback. '
---

