---
title: Accesibility
custom:
    content: "**How accessible is the website?**  \nWe are aware that parts of the website are not fully accessible. On this page, you may find information about existing accessibility  deficiencies. \n\n**Contact us if you find other accessibility problems**  \nWe are constantly striving to improve the website's accessibility. If you discover problems that are not described, or if you believe that we are not compliant with the current legislation, kindly let us know about it. You can leave [comments and feedback about digital accessibility here.](mailto:jobtechdev@arbetsformedlingen.se)\n\n**Contact the Contracting Authority**<br>\nThe Agency for Digital Government (Digg) is responsible for enforcing the Act on Access to Digital Public Service. If you are not satisfied with the way we respond to your viewpoints, you can [contact and inform the Agency for Digital Government](https://www.digg.se/tdosanmalan).\n\n**Technical information about the website’s accessibility**  \nNot all parts of this website comply with the Swedish [Act on the Accessibility of Digital Public Services](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/lag-20181937-om-tillganglighet-till-digital_sfs-2018-1937). Those parts that are not fully accessible are described as it follows below.\n\n**Content that is not accessible**  \nWe are aware that the content, described below, is in some way not fully accessible and compliant.  \n\n* Not all content is responsive\n* There are deficiencies in contrast\n\n**How did we test the website**  \nWe have conducted a self-assessment (internal testing) of jobtechdev.se.\n\n**How do we work with digital accessibility**  \nOur ambition is that our website will be accessible to all our users, including people with disabilities, so that everyone can perceive, understand, navigate, and interact with it. In addition, our ambition is to, at least, achieve basic accessibility by complying with the Web Content Accessibility Guidelines (WCAG 2.1) at level AA.\n\n<br><br><br><br>"
    menu:
        -
            title: 'Accesibility '
            url: /en/about-the-website/availability
        -
            title: 'Cookie policy (in Swedish)'
            url: /en/about-the-website/cookies
    title: 'Accesibility Summary'
    ingress: "## This website JobTech Development is under the auspices of the Swedish Public Employment Service (Arbetsförmedlingen). Our goal is that everyone will have access to the website, regardless of the needs. \n\nHere, you can find out how jobtechdev.se works in line with the current legislation on accessibility requirements from digital public sector bodies, possible accessibility problems and how you can report any deficiencies to us, so we can fix and always perform better. "
---

