---
title: 'Om webbplatsen'
custom:
    menu:
        -
            title: Tillgänglighet
            url: /sv/om-webbplatsen/availability
        -
            title: 'Cookie policy'
            url: /sv/about/cookies
    content: "Vårt mål för webbplatsen är att fullt ut uppfylla kriterierna i WCAG2.1-AA-standarden, vilket innebär att vi följer samma riktlinjer för tillgänglighetsanpassning som är lagkrav för myndigheter och andra offentliga organ.\n<br><br><br><br>"
    title: 'Om webbplatsen'
    ingress: 'Webbplatsen använder responsiv design, och fungerar bra i moderna webbläsare som stöder webbstandarder satta av W3C. Moderna webbläsare är bland andra Safari, Chrome och Firefox.'
routes:
    default: /om-webbplatsen
---

