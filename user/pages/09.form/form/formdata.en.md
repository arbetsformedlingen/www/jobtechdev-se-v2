---
simplesearch:
    process: false
title: Form
form:
    id: newsletter
    layout: newsletter
    name: newsletter
    fields:
        honeypot:
            type: honeypot
        email:
            label: 'Email address'
            placeholder: 'Email address'
            type: email
            validate:
                required: true
        checkbox:
            label: 'I agree that Arbetsförmedlingen stores the above information and shares information to me via email'
            type: checkbox
            validate:
                required: true
    buttons:
        submit:
            type: submit
            value: Subscribe
    process:
        email:
            from: '{{ config.plugins.email.from }}'
            to:
                - '{{ config.plugins.email.to }}'
                - '{{ form.value.email }}'
            subject: '[Newsletter] {{ form.value.email|e }}'
            body: '{% include ''partials/mailnewslettercontent.html.twig'' %} {% include ''forms/data.html.twig'' %} {{form|s}}'
        save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
            body: '{% include ''forms/data.txt.twig'' %}}'
        display: thankyou
---

