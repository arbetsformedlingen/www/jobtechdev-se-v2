---
title: 'Open Platforms'
custom:
    title: 'Open Platforms'
    description: 'A pilot project, aimed at making it easier for gig workers to use data that verifies the individual''s experience, generated from the gig.'
    contact_email: lisa.hemph@arbetsformedlingen.se
    contact_name: 'Lisa Hemph'
    block_1: "This pilot project is based on the hypothesis that if one makes it easier for the gig worker to use data that verifies his/her experience, generated from the gig, new paths and opportunities for matching, re-skilling and up-skilling will open up.\n\nA digital infrastructure for data portability in the gig economy can potentially solve some issues, existing partly at a system level and partly at an individual level. On one hand, today's problem at an individual level is that there is a limited opportunity for giggers to use data that verifies the gigger's experience, generated from the gig. This leads to and creates obstacles when the gigger is looking for work assignments in different contexts or forms. On the other hand, the problem at a system level is linked to slow convergence and increased risk of an inefficient matching market. Nowadays, the access to verified data experience is expensive for new actors in the matching market. Further, it increases the risks that the matching actors cannot reach out effectively with their tailored offers to job seekers. \n\nOpen Platforms is a pilot project, aiming at exploring the potential effects a digital infrastructure would have on the efficiency of the matching and the pace of adaptation of the re-skilling and up-skilling for the individual. Furthermore, the pilot project is aiming at looking at how the analysis and usage of data can facilitate the reshaping experience of work, confirming the gig worker's experience, gained from gig. \n\n* The benefit for the platforms is that they can facilitate and make it user-friendly for the users to get started with their matching offers.\n* The profit for gig workers is that they can increase the matching opportunities in a more effective way and thus the possibility for reshaping experience of work, re-skilling, and up-skilling, increases as well."
    block_2: "### More information\nRead more about the pilot projects we are currently working on at [www.openplatforms.org](https://www.openplatforms.org)."
    menu:
        -
            title: Website
            url: 'https://www.openplatforms.org/en/home'
            showInShort: '1'
        -
            title: Application
            url: 'https://www.mydigitalbackpack.org/en/home'
            showInShort: '1'
taxonomy:
    category:
        - Project
    type:
        - 'Open data'
    status:
        - Beta
---

