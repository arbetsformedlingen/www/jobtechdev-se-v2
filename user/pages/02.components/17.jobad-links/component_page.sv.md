---
title: 'JobAd Links'
custom:
    title: 'JobAd Links'
    description: 'Samlar, organiserar och länkar till jobbannonser från arbetsmarknadens aktörer'
    contact_name: 'JobTech Development'
    contact_email: jobtechdev@arbetsformedlingen.se
    block_1: "API:et JobAd Links har utvecklats genom ett samarbete med Sveriges största annons- och matchningsaktörer som fritt delar med sig av jobbannonsdata i ett [gemensamt digitalt ekosystem](https://jobtechdev.se/sv/komponenter/ekosystem_foer_annonser). JobAd Links är ett sök-API med stöd för fritextsökning och filter. Det ger tillgång till praktiskt taget alla jobbannonser på marknaden och möjliggör för användare att söka bland hela arbetsmarknadens utbud av annonser.\n\n## Hur går det till?\nAnnonserna samlas in och görs om till en gemensam standard för att kunna anpassas till flera tjänster. De berikas sedan med yrkes- och branschkoder, samt övriga metadata som behövs för att göra annonserna sökbara och maskinellt matchningsbara. Annonstexten reduceras så att endast ett par meningar görs tillgängliga – detta i enlighet med principen back-to-source, att föra trafiken tillbaka till ursprungskällan.\n\n\n\n\n\n"
    block_2: 'API:et JobAd Links genererar ytterligare 30% unika annonser, utöver de som publiceras på Platsbanken. Det gör det enklare för användare att söka bland hela arbetsmarknadens utbud av annonser, oavsett plattform. API:et hanterar och markerar även dubbletter, vilket varit ett problem för många användare. '
    menu:
        -
            title: API
            url: 'https://links.api.jobtechdev.se'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/joblinks'
            showInShort: '1'
    title_block_2: 'Vilket problem löser produkten?'
    title_block_3: 'Vem kan ha nytta av produkten?'
    block_3: 'Alla som vill ha tillgång till jobbannonser från hela marknadens utbud, exempelvis för matchningstjänster eller för forskning och statistik.'
    title_block_4: 'Medverkande annons- och matchningsaktörer'
    block_4: "* [arbetsformedlingen.se](https://arbetsformedlingen.se)\n* [jobb.blocket.se](https://jobb.blocket.se)\n* [careerbuilder.se](https://www.careerbuilder.se/?cbRecursionCnt=1)\n* [offentligajobb.se](https://www.offentligajobb.se)\n* [monster.se](https://www.monster.se)\n* [netjobs.com](https://www.netjobs.com)\n* [ingenjorsjobb.se](https://ingenjorsjobb.se)\n* [studentjob.se](https://www.studentjob.se)\n* [lararguiden.se](https://lararguiden.se)\n* [gronajobb.se](http://gronajobb.se/jobb/)\n* [thehub.io](https://thehub.io)\n* [jobbdirekt.se](https://jobbdirekt.se)\n* [onepartnergroup.se](https://www.onepartnergroup.se)\n* [saljpoolen.se](https://www.saljpoolen.se/sv)\n* [traineeguiden.se](https://www.traineeguiden.se)\n* [ingenjorsguiden.se](https://ingenjorsguiden.se)\n* [intenso.se](https://www.intenso.se/sv)"
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
media_order: image.png
---

