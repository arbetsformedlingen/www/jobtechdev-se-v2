---
title: Kompetensmatchning
custom:
    title: Kompetensmatchning.se
    description: 'Testbed and Demonstration Environment for Skills Development'
    block_1: "The environment has been initially developed as a platform focusing on the automotive industry in Gothenburg as a result of identifying the need of skills development and skills supply to enable the transition towards electric vehicles. As a result of the initial project, the environment has pivoted to become a testbed for the digital infrastructure, aiming at realistic performance testing automated solutions in an environment as close as possible to a production environment for the pilot projects.  \n\n## What problem does the product solve?\nKompetensmatchning.se serves as a marketplace for skills development where employers and education providers can collaborate and get insights on how to meet the employees’ needs in terms of skills supply and skills development.\n\n## For whom is the product created?\nKompetensmatchning.se can be useful to companies or organisations that must work or are already working with providing skills development for its employees. The testbed could also benefit players which have access to a large amount of structured data and would like to run realistic performance testing for ideas and market needs in an environment as close as possible to a production environment."
    block_2: 'Kompetensmatchning.se is a service tool developed by the state-owned RISE (Research Institutes of Sweden AB) in collaboration with Region Västra Götaland and Business Region Gothenburg. RISE offers unique expertise, about 100 testbeds, and demonstration environments for future-proof technologies, products, and services, among others.'
    menu:
        -
            title: Application
            url: 'https://kompetensmatchning.se/'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://github.com/LearningArena/competence-hub'
            showInShort: '1'
    product_info:
        -
            title: ' License'
            value: 'EUPL v1.2'
    contact_email: olle.nyman@ri.se
    contact_name: 'Olle Nyman'
taxonomy:
    category:
        - Application
    type:
        - 'Open source'
---

