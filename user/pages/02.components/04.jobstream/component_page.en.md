---
title: JobStream
custom:
    title: JobStream
    description: 'Real-time data from all job ads, published by the Swedish Public Employment Service (Arbetsförmedlingen).'
    block_1: "JobStream provides access to all job advertisements, published on the Swedish Public Employment Service’s Job board ([Platsbanken](https://arbetsformedlingen.se/platsbanken/)), including real-time information, related to changes, regarding these advertisements, e.g. publishing or deleting of job posts, or updates of job ads texts.\n\n## What problem does the product solve?\nBy using this API, it gives the opportunity to make a copy of all the job advertisements, published on the Swedish Public Employment Service’s Job board ([Platsbanken](https://arbetsformedlingen.se/platsbanken/)). It is also possible to filter by professions and/or themes of geography. \nFurthermore, it can be used to create an own search engine, or to conduct different types of analysis. The advertisements can be kept up-to-date via JobStream, while changes are saved in the own database, including expired and/or depublished job advertisements. \n\n## For whom is the product created?\nJobStream can be useful for companies and organisations that have their own search engine and are eager to improve their service with real-time updated job ads data. The API can also be used to build and train algorithms in relation to machine learning in order to identify and analyze trends on the labour market, based on job ads data.\n\nIn order to search and filter certain job advertisements, you may use [JobSearch.](https://www.jobtechdev.se/en/components/jobsearch)"
    menu:
        -
            title: API
            url: 'https://jobstream.api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobStreamEN.md'
            showInShort: '1'
        -
            title: 'Source code'
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis'
            showInShort: null
        -
            title: Forum
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/job-stream/25'
            showInShort: null
    product_info:
        -
            title: Version
            value: 1.17.1
        -
            title: 'Data format'
            value: JSON
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
taxonomy:
    category:
        - API
    type:
        - 'Open data'
---

