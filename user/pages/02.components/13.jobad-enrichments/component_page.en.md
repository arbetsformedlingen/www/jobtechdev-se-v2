---
title: 'JobAd Enrichments'
custom:
    description: 'AI solution that identifies and extracts relevant information in unstructured job advertisements.'
    contact_name: 'JobTech Development'
    version: '1.0'
    format: JSON
    licens: MIT
    publishdate: '19-05-2021 10:00'
    block_1: "JobAd Enrichments is an AI solution that automatically retrieves relevant words and phrases in job advertisements, while filtering out redundant information. The API contributes to a more accurate match between employers and job seekers, and makes it easier to navigate and quickly find a way on digital advertising platforms.\n\n## What problem does the product solve?\nIrrelevant search results are a common problem among users of digital matching services and advertising platforms. Being forced to spend time cleaning and sorting among search results makes job search more difficult. With JobAd Enrichments, problems can be minimized, and players, offering digital services, do not have to spend time and resources on manual management.\n\n## For whom is the product created?\nJobAd Enrichments could be useful for companies and organisations that offer digital matching services or advertising platforms, and would like to improve it. The API can also be used to develop new innovative digital services or to gain in-depth insight into the labour market."
    contact_email: jobtechdev@arbetsformedlingen.se
    title: 'JobAd Enrichments'
    menu:
        -
            title: API
            url: 'https://jobad-enrichments-api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://github.com/Jobtechdev-content/JobAdEnrichments-content/blob/master/GettingstartedJobAdEnrichmentsSE.md'
            showInShort: '1'
        -
            title: Forum
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/jobad-enrichments/26'
            showInShort: null
    product_info:
        -
            title: Version
            value: '1.0'
        -
            title: 'Data type'
            value: JSON
taxonomy:
    category:
        - API
    type:
        - 'Open data'
---

