---
title: 'Jobtech Taxonomy'
custom:
    title: 'Jobtech Taxonomy'
    description: 'Job titles, concepts of competence and the link between occupations and skills.'
    block_1: "JobTech Taxonomy provides access to the terms and concepts, which are used on the Swedish labour market, as well as information on how they are interconnected to each other. The nomenclature*'s content can refer, for example, to job titles, concepts of competence or the link between occupations/professions and skills. The job titles are structured according to the Swedish Standard Classification of Occupations (SSYK).\n\n## What problem does the product solve?\nUnclear, incomplete, or incorrect written job advertisements and CVs are a problem that can lead to a mismatch between employers and job seekers. As a result, they cannot find each other. The API creates a common language for the labour market, which makes it easier for employers and job seekers to understand and find each other through digital channels. It contributes to getting a more accurate matchmaking between the job seekers and the companies.\n \n## For whom is the product created?\nJobTech Taxonomy can be useful for companies and organisations that offer digital matching services, but do not have the opportunity to build an information structure on their own. The API is also intended for players, who are researching and analyzing the trends, while  following the development of the labour market.\n\n_*nomenclature refers broadly to taxonomy_\n"
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: API
            url: 'https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/index.html'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md'
            showInShort: '1'
        -
            title: Forum
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/jobtech-taxonomy/11'
            showInShort: null
    block_2: "### Please note\nThe nomenclature's content expands, polishes, and proofreads continuously with updated information, released in a controlled manner.<BR>\n    \n \n"
    product_info:
        -
            title: Version
            value: '1.0 (2.0 beta)'
taxonomy:
    category:
        - API
    type:
        - 'Open data'
        - 'Open source'
---

