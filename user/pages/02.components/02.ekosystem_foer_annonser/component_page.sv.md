---
title: ekosystem_för_annonser
custom:
    title: 'Ekosystem för annonser'
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    block_1: "Samarbetsprojektet ”Ekosystem för annonser” innebär att Sveriges största annons- och matchningsaktörer öppnar upp och delar med sig av jobbannonsdata i ett gemensamt digitalt ekosystem. Målsättningen är att göra det lättare för arbetssökande att navigera bland platsannonser och hitta jobb, oavsett plattform, samt att skapa ett större underlag till SCB:s arbetsmarknadsstatistik. Sedan februari 2021 testas den tekniska lösningen genom en pilot i Platsbanken.\n\nVinsterna med samarbetet är många:\n\n- Vi skapar bättre statistik på svensk arbetsmarknad\n- Vi gör det enklare för våra gemensamma användare att söka bland hela arbetsmarknadens utbud av annonser.\n- Vi lotsar användare till de externa webbplatser som deltar i ekosystemet."
    block_2: "## Vill du vara med?\nÄr du och din organisation intresserad av att ansluta till samarbetet? \n[Läs mer på vår WIKI-sida](https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home) eller [kontakta oss via e-post](mailto:jobtechdev@arbetsformedlingen.se).\n\n"
    description: 'Samlar jobbannonsdata från privata och offentliga aktörer i ett gemensamt digitalt ekosystem för bättre arbetsmarknadsstatistik.'
    menu:
        -
            title: 'Teknisk information'
            url: 'https://gitlab.com/arbetsformedlingen/joblinks/wiki/-/wikis/home'
            showInShort: '1'
taxonomy:
    category:
        - Projekt
    type:
        - 'Öppna data'
---

