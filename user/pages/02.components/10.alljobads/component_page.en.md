---
title: AllJobAds
custom:
    title: AllJobAds
    description: 'A widget that shows selected jobs from the Swedish Public Employment Service''s Job Board (Platsbanken).'
    block_1: "AllJobAds is a plug-and-play widget that displays and shows jobs from the [Swedish Public Employment Service's Job Board (Platsbanken)](https://arbetsformedlingen.se/platsbanken/). The widget can be set for specific areas of interest, and it is even possible to filter results.\n\n## What problem does the product solve?\nMany companies and organizations prefer to have a simple list of job vacancies available on their website. The widget is designed to be easily adjusted and it is tailored based on specific requests, while creating added value for the website's visitors.\n\n## For whom is the product created?\nAllJobAds is mainly targeted to companies and organizations that have a goal to guide people to get hired in a specific area, regardless of if it refers for example to municipality, company or profession."
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: Application
            url: 'https://widgets.jobtechdev.se/alljobads/'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/www/allJobAdsWidget'
            showInShort: '1'
taxonomy:
    category:
        - Application
    type:
        - 'Open source'
---

