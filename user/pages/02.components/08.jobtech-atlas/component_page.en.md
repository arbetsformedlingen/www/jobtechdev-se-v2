---
title: 'Jobtech Atlas'
custom:
    title: 'Jobtech Atlas'
    description: 'A user interface, which shows the data available in the product Jobtech Taxonomy.'
    block_1: "Jobtech Atlas is a website for users to explore the database behind the Jobtech Taxonomy. The database contains structures, concepts and relationships, which are used to describe the labour market.  \n\nOn the website, one can search among lists of concepts that often occur in job matching in the labour market, such as concepts of competence and skills, job titles, related to the structures of the Swedish Standard Classification of Occupations (SSYK). Moreover, one can find search terms that can be used to facilitate search and filtering. The nomenclature's (taxonomy) database is managed by the Swedish Employment Public Service and is based on information obtained by trade and industry organizations, employers, and job seekers, among others. The information is also available via the [API Jobtech Taxonomy](https://jobtechdev.se/en/components/jobtech-taxonomy). \n\n## What problem does the product solve?\nAn important part of the continuous work of producing and delivering data is to make data easily accessible. Jobtech Atlas makes the information, available in Jobtech Taxonomy, easily accessible in a format, which users can read and understand easier than, for instance, reading and understanding an API. \n\n## For whom is the product created?\nEveryone should be able to see and contribute to the language, used in the labour market. This information has previously been difficult to access for non-developers or people, outside the Swedish Employment Public Service. \n\nWe have built Jobtech Atlas in order to solve this problem and make the information available to anyone. "
    block_2: "### Kindly note\n\nThe project is set to undergo a further development and should not be recognized in its final phase. Features may be added, deleted or modified. The design is not fine-tuned and will probably be changed/improved.\n\nAt the moment, we have focused on making competences/skills and occupational employment data available. The remaining data will be available anytime soon. \n\nWe welcome any feedback."
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    product_info:
        -
            title: Version
            value: Beta
    menu:
        -
            title: 'User interface'
            url: 'https://atlas.jobtechdev.se/page/taxonomy.html'
            showInShort: '1'
        -
            title: 'Getting Started'
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas/-/blob/master/README.md'
            showInShort: '1'
taxonomy:
    category:
        - Application
    type:
        - 'Open data'
    status:
        - Beta
---

