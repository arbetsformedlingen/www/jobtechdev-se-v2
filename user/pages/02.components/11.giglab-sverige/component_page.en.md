---
title: 'Giglab Sverige'
custom:
    title: 'Giglab Sverige'
    description: 'A National Policy Lab and Forum, aiming at highlighting challenges from different perspectives in the gig economy and the future labour market '
    block_1: "Giglab Sverige is a Swedish national policy lab and a collaboration initiative between Jobtech Development (Swedish Public Employment Service), The Swedish Tax Agency, Coompanion and the Stockholm School of Economics together with SVID (The Swedish Industrial Design Foundation; in Swedish: Stiftelsen Svensk Industridesign) and the Swedish labnetwork (design, policy and innovation lab). Some activities of the lab are a part of the Vinnova call \"Challenge-driven Innovation\". Our ambition is to work together towards a sustainable growth of the gig economy in Sweden.\n\n## Objective\n* Sole basis for a test bed of new prototype sustainable solutions in the gig economy\n* Sole basis for the decision makers in the continuous discussion for policy and legislation amendments, related to gig economy\n* Input in the ongoing work in relation to Agenda 2030; focus on better working conditions and digital infrastructure, related to the gig economy\n* Sole basis for the continuous internal work of various authorities, related to the gig economy\n* Empirical basis for research in the field of gig economics, which for instance is conducted by the postdoc researcher Claire Ingram Bogusz at the Stockholm School of Economics\n\n## Method\nGiglab Sverige is a first joint learning project, aiming at investigating the possibilities to contribute to creating a sustainable future labour market, which includes sustainable gigging, creates value for the society, for the individual and it is applicable for different business models in a digital and global world."
    menu:
        -
            title: Website
            url: 'https://www.giglabsverige.se'
            showInShort: '1'
    block_2: "### Are you a gigger, a platform owner or in any other way in contact with the gig economy?\nWe would gladly appreciate your kind contribution with insights, perspectives and challenges for mapping the gig economy in Sweden. Read more about how you can assist us on our [website](https://www.giglabsverige.se)."
    contact_email: lisa.hemph@arbetsformedlingen.se
    contact_name: 'Lisa Hemph'
taxonomy:
    category:
        - Project
    type:
        - 'Open research'
---

