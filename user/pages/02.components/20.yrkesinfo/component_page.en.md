---
title: 'Occupations Info'
custom:
    title: 'Occupations Info'
    description: 'Approximately 350 job descriptions with e.g. work tasks, working hours, work environment, etc.'
    block_2: "**Important Information**\n \nOccupations Info does not have a comprehensive relevance concerning the Swedish Standard Classification of Occupations (SSYK) of Statistics Sweden. Occupation Info contains customized occupational groups with merged occupations, available in this API. The information is to be updated continuously and regularly.\n\n"
    block_1: "Occupations Info provides access to descriptive texts about occupations such as work tasks, working hours, work environment, etc. Occupations Info is outlined and structured according to the Swedish Standard Classification of Occupations (SSYK), apart from certain customized occupational groups.\n\nThe customized occupational groups are organized and provided manually, which means that certain occupations are merged and therefore differ from the Swedish Standard Classification of Occupations (SSYK). The manual material and information processing implies that updates take place on an ongoing basis and vary in scope and size.\n\n## For whom is the product created?\nThe API can be useful for companies and organisations that provide vocational, study, and career guidance. In particular, if they would like to assist people making better-informed decisions while choosing a future career or educational option that matches up with their goals and the market’s needs.\n \nThe API is also intended for players interested in getting more descriptive and explanatory texts about the various occupations in the labour market.\n\n\n"
    contact_email: jobtechdev@arbetsförmedlingen.se
    contact_name: 'JobTech Development'
published: false
---

