---
title: Yrkesinfo
custom:
    title: Yrkesinfo
    description: 'Cirka 350 yrkesbeskrivningar med till exempel arbetsuppgifter, arbetstider, arbetsmiljö'
    block_2: "Viktig information\n\nYrkesinfo är inte heltäckande i förhållande till SCB´s Standard för Svensk Yrkesklassificering (SSYK). Yrkesinfo innehåller anpassade yrkesgrupper vilket gör att vissa sammanslagningar finns i detta API. Uppdateringar av information sker löpande.\n\n"
    block_1: "Yrkesinfo ger tillgång till beskrivande texter om yrken som tex. arbetsuppgifter, arbetstider, arbetsmiljö mm. Yrkesinfo är strukturerad enligt Standard för Svensk Yrkesklassificering (SSYK), bortsett från vissa anpassade yrkesgrupper. \n\nDe anpassade yrkesgrupperna tas fram i ett manuellt arbete vilket innebär att det förkommer sammanslagningar som skiljer sig från Standard för Svensk Yrkesklassificering (SSYK). Den manuella bearbetningen av materialet innebär att uppdateringar sker löpande i varierande storleksgrad.\n\n\n## Vem kan ha nytta av produkten?\nAPI:et är användbart för alla företag och organisationer som erbjuder tjänster inom yrkes- och studievägledning och som vill hjälpa sina målgrupper att fatta välgrundade beslut inför yrkes- och studieval. Aktörer som vill ha förklarande texter om de olika yrkena på arbetsmarknaden har också nytta av API:et.\n\n\n"
    contact_email: jobtechdev@arbetsförmedlingen.se
    contact_name: 'JobTech Development'
published: false
---

