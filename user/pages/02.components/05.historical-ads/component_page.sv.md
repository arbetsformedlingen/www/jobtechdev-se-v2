---
title: 'Historiska annonser'
custom:
    title: 'Historiska annonser'
    description: 'Ger en översiktlig bild på den svenska arbetsmarknaden och hur behoven har förändrats över tid.'
    contact_email: jobtechdev@arbetsformedlingen.se
    contact_name: 'JobTech Development'
    menu:
        -
            title: API
            url: 'https://historical.api.jobtechdev.se'
            showInShort: '1'
        -
            title: Dataset
            url: 'https://data.jobtechdev.se/annonser/historiska/index.html'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/job-ads/getting-started-code-examples/historical-ads-info'
            showInShort: '1'
        -
            title: Diskutera
            url: 'https://forum.jobtechdev.se/c/vara-api-er-dataset/historiska-annonser/30'
            showInShort: null
    block_1: "## Dataset \nHistoriska jobb är ett dataset med alla jobbannonser som har publicerats på Arbetsförmedlingens Platsbanken sedan 2006. Det innehåller data som finns i annonser, t ex information om tjänst, arbetsgivare, ort, tidpunkt, osv. Datasetet innehåller cirka 6,9 miljoner annonser. Det uppdateras kontinuerligt och förenklar processer för att bygga och träna algoritmer i samband med maskininlärning.\n\n## API:et \nAPI:et Historiska Annonser använder datasetet för historiska annonser och återanvänder koden för [JobSearch](https://jobtechdev.se/sv/komponenter/jobsearch) för att bygga ett nytt sök för alla jobbannonser som har publicerats på Arbetsförmedlingens Platsbanken sedan 2006. Den första betaversionen omfattar data mellan 2016-2021. Alla annonser är berikade med kompetenser, samt innehåller data om distansarbete. API:et Historiska Annonser underlättar för dem som vill använda arbetsmarknadsdata på ett enkelt, begripligt och strukturerat sätt.\_API:et är lätt att testa och ger möjligheten att snabbt ladda ner stora mängder data. Idag omfattar API:et cirka 3,3 miljoner annonser.\n\n## Vilket problem löser produkten?\nMed hjälp av datan kan man jämföra tidsperioder, samt analysera och följa trender inom arbetsmarknadsområdet. \n \n## Vem kan ha nytta av produkten?\nDatan är användbart för alla företag, organisationer, privata personer som vill använda historisk annonsdata för analyser och fånga upp trender inom arbetsmarknadsområdet."
    block_2: "API:et och datasetet är kostnadsfritt och tillgängligt för alla. \nVill du veta mer om Historiska annonser, kontakta gärna oss!\_"
    product_info:
        -
            title: Dataformat
            value: JSON
        -
            title: Version
            value: '1.23.1 Beta'
taxonomy:
    category:
        - API
        - Dataset
    type:
        - 'Öppna data'
    status:
        - Beta
---

