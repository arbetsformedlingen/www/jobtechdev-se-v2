---
title: digitalskills-se
custom:
    title: Digitalskills.se
    description: 'Ett prognosverktyg som ger både historiska trender samt framskrivningar på digitala kompetenser och yrkestitlar genom att datadrivet analysera jobbannonser'
    contact_email: kontakt@assedon.se
    contact_name: Assedon
    block_1: "Vilka tech-kompetenser trendar just nu? Vilka yrkestitlar inom data/it har växt mest den senaste tiden och vilka beräknas vara de mest eftertraktade om 18 månader? Digitalskills.se är ett prognosverktyg som ger både historiska trender samt framskrivningar på digitala kompetenser och yrkestitlar genom att datadrivet analysera jobbannonser. Annonsanalysen presenteras i ett smidigt gränssnitt där du kan se topplistor, vilka arbetsgivare som efterfrågar vad och var i Sverige efterfrågan finns. Digitalskills.se är utvecklat av företaget Assedon och framtagen inom ett [regeringsuppdrag kring kompetensförsörjning av digital spetskompetens](https://digitalspetskompetens.se) som utförs av Tillväxtverket och Universitetskanslersämbetet (UKÄ).\n\n## För vem är produkten skapad?\nDet finns flera tänkbara målgrupper för verktyget men de mest framträdande är t.ex. utbildningsanordnare av yrkesutbildning, kompetensförsörjningsråd och studie- och yrkesvägledare som på olika sätt behöver förankra efterfrågan på arbetsmarknaden i sin verksamhet.\n\n## Hur fungerar produkten?\n**1. Extrahering av annonser från rådata**<BR>\n[Historiska annonser](https://jobtechdev.se/sv/komponenter/historical-ads) hämtas kvartalsvis. Från datasetet extraheras annonser som är kategoriserade som Data/IT enligt [Jobtech taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy). \n\n**2. Berikning av annonsdata**<BR>\n[JobAd Enrichments](https://jobtechdev.se/sv/komponenter/jobad-enrichments) anropas via API och tillämpas på aktuella annonser för att plocka fram kompetenser, yrken, information om jobbets geografiska läge samt mjuka kompetenser kopplade till annonsen. Extraheringen av kompetenser tar ej i beaktning avgränsningen till Data/IT och följaktligen behöver resultatet filtreras genom en framtagen ”black list” på kompetensord. Processen har ett tröskelvärde för den statistiska säkerhet som [JobAd Enrichments](https://jobtechdev.se/sv/komponenter/jobad-enrichments) anser sig ha i sin träffsäkerhet för extraheringen av de olika datapunkterna.\n\n**3. Framskrivning av data**<BR>\nAnnonserna passerar sedan en funktion som med hjälp av öppna bibliotek i Python applicerar algoritmer för prognostisering av tidsserier. Metoden som tillämpas är exponentiell utjämning där tidsseriens säsongsvariation först utreds för att sedan tas i beaktning i framskrivningen. Framskrivningar för 6, 12 och 18 månader tas fram och sparas ned som tidsserier i resultatobjektet. I samma steg sparas även värden för historiska trender på samma intervall.\n    \nVill du läsa mer om hur digitalskills.se är utvecklat, ladda ner [utvecklingsrapporten](https://jobtechdev.se/user/pages/02.components/18.digital-skills/Utvecklingsrapport.pdf)."
    menu:
        -
            title: 'Webbplats '
            url: 'https://www.digitalskills.se'
            showInShort: '1'
        -
            title: API
            url: 'https://dig-api-kbrvfttzua-uc.a.run.app/'
            showInShort: '1'
        -
            title: Källkod
            url: 'https://github.com/Assedon-AB/digitalskills.se '
            showInShort: null
    block_2: "## Viktigt\nDet är viktigt att komma ihåg att prognoser i sin grund är osäkra och bör betraktas med en viss försiktighet. Det finns kvarstående utmaningar med det underliggande datasetet samt den tillämpade metoden som verktyget är byggt på som kan leda till missvisande information eller direkta felaktigheter. Vi rekommenderar därför att du använder verktyget som en av flera metoder i din omvärldsbevakning. Komplettera gärna datadrivna metoder som den här med kvalitativa branschrapporter och intervjuer av arbetsgivare."
    product_info:
        -
            title: Version
            value: '1.0.0 Beta'
        -
            title: Frontend
            value: Next.js
        -
            title: Backend
            value: Python
media_order: Utvecklingsrapport.pdf
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
        - 'Öppen källkod'
---

