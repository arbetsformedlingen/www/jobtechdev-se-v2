---
title: 'JobTech Development at "Public Sector Innovations 2022" '
custom:
    title: 'JobTech Development Will Be Participating at "Public Sector Innovations 2022" June, 8th-9th'
    date: '2022-06-08 09:00'
    endtime: '2022-06-09 17:00'
    short: 'Welcome to visit us at Sweden''s largest public sector''s conference  "Public Sector Innovations" on June, 8th-9th'
    content: "###### Welcome to visit us at Sweden's largest public sector's conference  \"Public Sector Innovations 2022\" on June, 8th-9th. \n\nCome and visit us at our stand M: 04 and listen to our speaker on June 8th at 13:45. We will be talking more about who we are and how to use our services.\n\nAmong others, we will be talking about historical data and how to use the dataset and the API to get an overview of what have been the most wanted skills in your particular municipality during the last year / years.\n\nBy using our service, you will be able to follow trends, and therefore map easier the needs of the future labour market. \n\nCome by and visit us, we will be happy to tell your more about what we do. \n\n[More info about the conference](https://losningarforoffentligsektor.se/)\n"
published: true
taxonomy:
    category:
        - Event
    status:
        - Completed
---

