---
title: 'Utdrag av det franska EU-ordförandeskapets programmet'
custom:
    title: 'Det franska EU-ordförandeskapets program: viktiga punkter avseende arbetsmarknad, digitalisering och livslångt lärande'
    content: "* Ordförandeskapet kommer att prioritera stora ekonomiska övergångar för att stödja **sysselsättningen** i samband med den europeiska återhämtningen. Man kommer att arbeta för att rådet ska anta rekommendationer om individuella **lärandekonton** och om klimatomställningens sociala inverkan och effekter på arbetsmarknaden.\n* Man kommer därför att ägna särskild uppmärksamhet åt frågorna om **livslång utbildning, sysselsättningsövergångar och social dialog**, särskilt vid det Sociala trepartstoppmötet, för att ge stöd under de förändringar som de digitala och klimatmässiga omställningarna medför.\n* Man kommer att sträva efter att främja **ett kunskapsbaserat och industriellt Europa** som är mer suveränt, mer innovativt och som gör framsteg i den digitala och gröna omställningen.\n* Ordförandeskapet kommer att prioritera reformen av den **digitala världen**. När det gäller reglering av innehåll kommer man att gå så långt som möjligt med samtal med Europaparlamentet om **Lagen om digitala tjänster** (Digital Services Act – DSA), vilken innebär ett tydligt, effektivt och proportionerligt rättsligt ramverk. När det gäller den ekonomiska regleringen av stora digitala plattformar kommer det franska ordförandeskapet att fortsätta förhandlingarna om **Rättsakten om digitala marknader** (Digital Markets Act – DMA).\n* Man kommer att prioritera ett antal strategiska områden för **europeisk digital suveränitet kopplade till dataskydd, utveckling av artificiell intelligens, säkerhet och förstärkning av nätverk och infrastruktur** för större motståndskraft.\n* Man kommer att fortsätta arbetet med **e-integritetsförordningen** angående individens privatliv och skyddet av personuppgifter i elektronisk kommunikation, som förtydligar och kompletterar den allmänna dataskyddsförordningen (GDPR) och samtidigt skyddar individers och juridiska organisationers rättigheter och friheter med avseende på tillhandahållande och användning av elektroniska kommunikationstjänster.\n* **Datadelning och storskalig databehandling** – som möjliggör tillväxt, innovation och konkurrenskraft för företag och forskningscenter – kan också vägleda den offentliga politiken för myndigheter.\n* Man kommer att påbörja arbetet med **datalagen** (Data Act), som tillsammans med den redan antagna **datastyrningsförordningen** (Data Governance Act) ska bidra till att skapa ett tillförlitligt rättsligt ramverk som möjliggör datautbyte samtidigt som den säkerställer att delningsmekanismerna är säkra.\n* För att skapa praktiska resultat för europeiska medborgare kommer ordförandeskapet att fortsätta arbetet med att skapa en **europeisk digital identitet **för att främja pålitliga digitala identiteter för alla européer.\n* Projektarbetet med **nästa generations molninfrastruktur och molntjänster** är av gemensamt europeiskt intresse, och kommer därför att vara ett led i att stödja utvecklingen av den europeiska molninfrastrukturen och de tjänster som behövs för den digitala omställningen.\n* Ordförandeskapet kommer att sträva efter att stärka **det europeiska samarbetet** genom det digitala politiska program som föreslås av kommissionen och en framtida interinstitutionell deklaration som fastställer unionens övergripande principer på det digitala området. \n\nHela [programmet av det franska EU-ordförandeskapet](https://presidence-francaise.consilium.europa.eu/media/qh4cg0qq/en_programme-pfue-v1-2.pdf) (på engelska) hittar man via länken.  \n"
    short: 'Det franska EU-ordförandeskapets program: viktiga punkter avseende arbetsmarknad, digitalisering och livslångt lärande'
taxonomy:
    type:
        - Insikt
---

