---
title: 'Jobtech at API Days Helsinki & North 2022'
custom:
    title: 'Jobtech at API Days Helsinki & North 2022'
    short: 'Our colleague Jonas Södergren will be talking about one Public Sector’s “Open” Story: Sweden API Award Winner JobStream on March 16th at 10.05 CET.'
    content: "> “I am fascinated by new technologies, open data, and the endless possibilities to achieve a solid social impact through collaborations and sharing data”, says Jonas Södergren, Technical Product and Procurement Manager, and programmer at JobTech Development. \n\nJob Tech Development will be participating at the virtual conference API Days Helsinki & North 2022. The event takes place on March 16th-17th 2022. Our colleague Jonas Södergren will be among the strong line-up speakers at this year’s event. He will be talking about one Public Sector’s “Open” Story: Sweden API Award Winner JobStream with start on March 16th at 10.05 CET. \n\nJobStream is an appreciated open API for job ads, by the Swedish Public Employment Service. It is free of charge, well utilized and creates value for the labor market. Learn more about the [API JobStream here](https://jobtechdev.se/en/components/jobstream). \n\n> “We are working to create a sustainable and common infrastructure for digital matching services in Sweden. Our purpose is to facilitate a relevant match between job seekers and employers by sharing data and open source, and to contribute to a more skills-based and data-driven labour market“, says Jonas Södergren. \n \nCome and join us [here](https://hopin.com/events/apidays-helsinki-north-2022). \n\n**About API Days Helsinki & North **\n\nApi Days Helsinki & North joins the tradition of hugely popular global API events. It is an annual conference, which gathers participants from various big and small companies, mostly in the Nordics, Europe and US.  \n\nThe event is free for all participants and gathers over 1000 participants from Northern Europe and around the world. More information about the [event.](https://www.apidays.global/helsinki_and_north/ ) \n"
taxonomy:
    category:
        - News
date: '2022-03-09 11:56'
---

