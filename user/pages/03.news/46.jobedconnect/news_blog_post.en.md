---
title: 'New API: JobEd Connect'
custom:
    title: 'New API: JobEd Connect'
    short: 'Today, we launch JobEd Connect, an open API that facilitates the navigation between education and occupations to match relevant education to relevant careers and employment.'
    content: "**Today, we launch JobEd Connect, an open API that facilitates the navigation between education and occupations to match relevant education to relevant careers and employment. **\n\nBy connecting two central datasets from the labour and education markets, we have identified a new technology area between jobtech and edtech. We call it JobEdTech, where we are bridging skills supply and lifelong learning to foster sustainable matching in the labour market.\n\nThe new API JobEd Connect is a solution that makes it possible to provide digital career guidance by automatically linking education programs to in-demand competencies, most often searched for by employers for a specific occupation. It is also possible to find out which education program is related to a greater extent to a specific job occupation and its most particular in-demand skills. The solution is developed primarily on data from the National Education Database SUSA Hub (_in Swedish SUSA-navet_) and from enriched historical job ads.\n\nJobTech Development has developed JobEd Connect with the help of open data to benefit tomorrow's rapidly and constantly changing labour market, where relevant education, continuous learning, and skills development are everyone's keys to success. \n\n[Read more and test JobEd Connect here.  ](https://jobtechdev.se/en/components/jobed-connect)\n\n"
date: '2023-03-14 10:40'
taxonomy:
    category:
        - News
---

