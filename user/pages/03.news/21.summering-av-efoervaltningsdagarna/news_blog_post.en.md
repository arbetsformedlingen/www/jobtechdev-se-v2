---
title: 'Summary of eGoverment Days (eFörvaltningsdagarna)'
custom:
    content: "**Forces of change are needed at all levels for a successful digital transformation**\n\nJobTech Development participated in the eGovernment Days October 27-28, where we exchanged experiences about open data and open source code. Lecturers and panelists from both the public and private sectors agreed upon the importance of collaboration on all levels to boost innovation towards digitalization.\n\nJonas Södergren from JobTech Development was among the speakers on stage, addressing tomorrow’s labour market and how and with what JobTech contributes in this direction.\n\nThere are a lot of takeaways from these intensive days. However, one key takeaway is that nothing can compare being at an event and meeting people in reality.\n\nStay tuned!"
    title: 'Summary of eGoverment Days (eFörvaltningsdagarna)'
    date: '2021-10-29 11:41'
    short: 'JobTech Development participated in the eGovernment Days October 27-28, where we exchanged experiences about open data and open source code. '
date: '2021-10-29 11:41'
taxonomy:
    category:
        - News
---

