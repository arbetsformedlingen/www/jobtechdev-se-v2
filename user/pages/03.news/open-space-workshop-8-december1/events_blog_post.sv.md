---
title: 'Open Space workshop del 1'
custom:
    title: 'Open Space workshop den 8 december'
    description: 'Var med och skapa framtidens community för den svenska utbildnings- och arbetsmarknaden. JobTech tillsammans med SUNET inleder serieworkshops i formatet "Open Space" om digitalisering, kompetensförsörjning och livslångt lärande. Vi välkomnar alla inom såväl offentlig som privat sektor att delta och bidra med insikter, förslag och tankar, med start 8 december 10-17.  <a id="link" href="https://lnkd.in/ejnz7DBC"> Registrera dig här</a>'
    short: 'JobTech tillsammans med SUNET inleder serieworkshops i formatet "Open Space" om digitalisering, kompetensförsörjning och livslångtlärande.'
    date: '2021-12-08 10:00'
    endtime: '2021-12-08 17:00'
    content: "Var med och skapa framtidens community för den svenska utbildnings- och arbetsmarknaden. JobTech tillsammans med SUNET inleder serieworkshops i formatet \"Open Space\" om digitalisering, kompetensförsörjning och livslångt lärande. Vi välkomnar alla inom såväl offentlig som privat sektor att delta och bidra med insikter, förslag och tankar, med start 8 december 10-17. \n\n[Registrera dig här](https://lnkd.in/ejnz7DBC)"
taxonomy:
    category:
        - Event
    status:
        - Avslutad
published: true
---

