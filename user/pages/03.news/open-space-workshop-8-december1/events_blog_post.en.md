---
title: 'Open Space Workshop Vol.1'
custom:
    title: 'Open Space Workshop on December 8th, 2021'
    date: '2021-12-08 10:00'
    endtime: '2021-12-08 17:00'
    short: 'JobTech together with SUNET, is launching a serie of workshops in the "Open Space" format on digitalization, skills supply and lifelong learning.'
    content: "Come and join us in creating the future community for the Swedish education and labour market. \n\nJobTech together with SUNET is launching a serie workshops in the \"Open Space\" format on digitalization, skills supply and lifelong-learning.  We welcome everyone in the public and private sectors to participate and contribute with insights, suggestions and thoughts, starting on December 8th, 2021, at 10:00-17:00.\n\n[For registeration](https://lnkd.in/ejnz7DBC)"
taxonomy:
    category:
        - Event
    status:
        - Completed
published: true
---

