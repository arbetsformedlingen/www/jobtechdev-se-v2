---
title: 'JobTech Development deltar på  "Lösningar för offentlig sektor" '
custom:
    title: 'JobTech Development deltar på "Lösningar för offentlig sektor" 8-9 juni'
    date: '2022-06-08 09:00'
    endtime: '2022-06-09 17:00'
    short: 'Välkommen att besöka oss på Sveriges största konferens för offentlig verksamhet den 8-9 juni om ”Lösningar för offentlig sektor”'
    content: "###### Välkommen att besöka oss på Sveriges största konferens för offentlig verksamhet den 8-9 juni om ”Lösningar för offentlig sektor”. \nVi kommer stå i monter M:04 och även ha en talarslot på scenen intill den stora konferenssalen den 8 juni kl 13:45, där vi berättar mer om vilka vi är och hur man kan använda våra tjänster\n\nI montern kommer vi bland annat att prata historiska data och hur man kan nyttja denna tjänst för att få en överblick vilka som har varit de mest efterfrågade kompetenserna i just din kommun under de sista året/åren.  \nDenna tjänst kan vara till stor nytta för att kartlägga behoven på arbetsmarknaden framåt. \nKom förbi och hälsa på så får du veta mer. \n\n[mer info om konferensen](https://losningarforoffentligsektor.se/)\n"
published: true
taxonomy:
    category:
        - Event
    status:
        - Avslutad
---

