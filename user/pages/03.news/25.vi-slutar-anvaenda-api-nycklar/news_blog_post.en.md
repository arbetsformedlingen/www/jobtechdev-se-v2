---
title: 'We No Longer Use API Keys'
custom:
    title: 'We No Longer Use API Keys'
    short: 'From now on, keys are no longer required to access APIs on the JobTech Development''s platform.'
    content: "From now on, unique keys are no longer required to access and use APIs on the JobTech Development's platform. For us, it is important that the information is open and that anyone who wants and can be anonymous as a user. Optimizing accessibility is a democratic question, and it means that it will be easier for users to test APIs without the lead times that the key management entails.\n \nCurrently, two APIs still require keys: JobSearch and JobStream. Therefore, we have developed a general key that works for all users.\n\n* JobSearch: developer\n* JobStream: developer\n\nIf there is any problem, linked to the fact that we have removed the keys, do not hesitate to ask for assistance at our [forum](https://forum.jobtechdev.se/t/vi-slutar-anvanda-api-nycklar/565)."
taxonomy:
    category:
        - News
date: '2022-03-04 08:59'
---

