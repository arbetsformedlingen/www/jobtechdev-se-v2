---
title: 'Vi slutar använda API-nycklar '
custom:
    title: 'Vi slutar använda API-nycklar '
    short: 'Från och med nu krävs inte längre nycklar för att få tillgång till API:er på JobTech Developments plattform.'
    content: "Från och med nu krävs inte längre unika nycklar för att få tillgång till API:er på JobTech Developments plattform. För oss är det viktigt att informationen är öppen och att den som vill ska få vara anonym som användare. Att tillgängligheten maximeras är en demokratisk fråga, och innebär också att det blir enkelt för användare att testa API:er utan de ledtider som nyckelhantering innebär.\n \nI dagsläget kräver två API:er fortfarande nycklar: JobSearch och JobStream. För dessa har vi tagit fram en generell nyckel som fungerar för alla användare.\n\n* JobSearch: developer\n* JobStream: developer\n\nOm något problem dyker upp, kopplat till att vi plockat bort nycklarna, så finns alltid hjälp att få i [forumet](https://forum.jobtechdev.se/t/vi-slutar-anvanda-api-nycklar/565)."
taxonomy:
    category:
        - Nyhet
date: '2022-03-04 08:58'
---

