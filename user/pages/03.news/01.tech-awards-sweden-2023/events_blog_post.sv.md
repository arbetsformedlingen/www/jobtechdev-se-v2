---
title: 'Tech Awards Sweden 2023'
custom:
    title: 'Tech Awards Sweden 2023'
    date: '2023-03-22 13:00'
    endtime: '2023-03-22 22:00'
    short: 'Här samlas innovatörerna och de starka krafterna som påverkar, förändrar och lyfter traditionella sektorer – och banar väg för nya. Därför vill Foundry och TechSverige premiera personerna, företagen, organisationerna och initiativen genom den årliga utmärkelsen Tech Awards, med prisutdelning och fest.'
    content: "## Här samlas innovatörerna och de starka krafterna som påverkar, förändrar och lyfter traditionella sektorer – och banar väg för nya. Därför vill Foundry och TechSverige premiera personerna, företagen, organisationerna och initiativen genom den årliga utmärkelsen Tech Awards, med prisutdelning och fest.\n## \nDen 22 mars 2023 blir det mingel och party för att hylla hjältar inom tech och fira framgångarna med en prisutdelning i sex kategorier.\n\nPriser som kommer delas ut i kategorierna:\n\n* Årets Säkerhetspris\n* Årets Kompetenspris\n* Tech50 – Mest Inflytesrika inom Tech\n* Årets Mest Värdefulla Användning av Tech\n* Årets Hållbarhetspris\n* Årets Techföretag\n\nVälkommen till Tech Awards 2023!\n\n[Mer info och anmälan](https://techawards.se/)\n\n\n"
date: '2023-02-06 08:13'
taxonomy:
    category:
        - Event
---

