---
title: 'Important Information about the API JobSearch'
custom:
    title: 'Important Information about the API JobSearch'
    short: 'The endpoint Taxonomy/Search will be removed by the end of September 2022. '
    content: "The endpoint Taxonomy/Search will be removed by the end of September 2022. To be able to browse and search in the nomenclatures (taxonomies), we are kindly refering to the [API JobTech Taxonomy](https://jobtechdev.se/en/components/jobtech-taxonomy), where it is also possible to use GraphQL.\n\nIf any further questions should appear, feel free to ask and comment in our [Community Forum](https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28)."
taxonomy:
    category:
        - News
    type:
        - Information
date: '2022-09-09 11:11'
---

