---
title: 'Open Space Vol.3 '
custom:
    title: 'Open Space Vol.3'
    date: '2022-06-01 10:30'
    endtime: '2022-06-02 15:00'
    short: 'JobTech together with Sunet welcome you to the third workshop in the format  "Open Space"  about digitalization, skills supply and lifelong learning. '
    content: "**It's time again. Join us to continue building the future community. Welcome to our next workshop Open Space Vol. 3 about  digitalization, skills supply and lifelong learning. The event will take place on June, 1st-2nd 2022.**\n\nWe welcome everyone in both the public and private sectors to participate, contribute and collaborate. If you have colleagues or other peers, who may be interested in joining us, feel free to invite them. We need both your's and your peers' wise insights, suggestions and thoughts for contributing to an event, which creates added value and maximizes benefits for everyone. We know that collaborative iniatives give concrete results.\n\nWe welcome you to take part in the workshop and to be the one who creates the agenda on site. \n\nIt will be educational, rewarding and fun.\n\n**Place and time**\n\nThe event will be run in 2 days, on June 1st -2nd 2022, at the The Swedish Internet Foundation's premises at Hammarby Kaj 10D in Stockholm. \n\n[Sign up](https://www.eventbrite.se/e/open-space-3-digitalisering-kompetensforsorjning-och-livslangt-larande-registrering-311068152217) and read more about the event.\n\nThe last day for registration is Monday, May, 23rd. Please, note that we have limited spots, so first come, first served. We offer lunch and coffee both days.\n\nWe hope to see you there!\n"
taxonomy:
    category:
        - Event
    status:
        - Completed
---

