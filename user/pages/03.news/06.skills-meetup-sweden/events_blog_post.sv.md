---
title: 'Skills Meetup Sweden 19-21 april'
custom:
    title: 'Skills Meetup Sweden 19-21 april'
    date: '2023-04-19 11:00'
    endtime: '2023-04-21 16:00'
    short: 'Välkommen till en mötesplats för erfarenhetsutbyte, nätverkande och lärande kopplat till aktuella frågor för en effektiv kompetensförsörjning. Eventet är arrangerat av Myndigheten för yrkeshögskolan och EU-projekten BOSS, NOVA-Nordic och SeQF 3.0 samt projektet Kompetenspasset.'
    content: "Hur kan vi lösa kompetensbristen på svensk arbetsmarknad? Hur kan vi bygga hållbar infrastruktur för livslångt lärande? Vad behövs för att stärka arbetsliv och individer för en ökad konkurrenskraft?\nVälkommen till en mötesplats för erfarenhetsutbyte, nätverkande och lärande kopplat till aktuella frågor för en effektiv kompetensförsörjning.\n\nMötesplatsen är till för dig som arbetar med frågor som rör kompetensförsörjning och livslångt lärande på nationell, regional och lokal nivå. Kvalifikationer, mikromeriter, yrkesutbildning och validering inom både utbildning och arbetsliv är i fokus. Dagarna kommer även att erbjuda möjlighet till internationell utblick kopplad till EU:s Skills Agenda.\n\nPlats: Quality Hotel View, Hyllie, Malmö.\n\n[Mer detaljer och anmälan: ](https://app.bwz.se/myh/b/v?event=18&ucrc=D3BC7EAD8D)"
date: '2023-03-28 11:58'
taxonomy:
    category:
        - Event
---

