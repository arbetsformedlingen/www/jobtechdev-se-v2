---
title: 'Sweden Innovation Days March, 21st-23rd'
custom:
    title: 'Sweden Innovation Days March, 21st-23rd'
    date: '2023-03-21 14:00'
    endtime: '2023-03-23 17:00'
    short: 'Sweden Innovation Days is a free of charge digital conference, which will focus on how to enable innovation that has a real impact on our society, environment and economy.'
    content: "**Sweden Innovation Days is a free of charge digital conference, which will focus on how to enable innovation that has a real impact on our society, environment and economy.\n**\n\nThe event is dedicated to fostering international collaboration and it is open for startups, public and private sector, civil society, academia.\n\nThe Swedish Public Employment Service (SPES) will take part in the conference. Among this year's speakers line is Linda Schön Doroci, Business Area Director of Direct Services at SPES. As a leader, Linda dedicates her work to focus on long-term skills supply and lifelong learning together with other actors and stakeholders in the ecosystem for a solid customer and societal impact. \n\n[Registration and details](https://swedeninnovationdays.se/)"
date: '2023-01-13 11:11'
taxonomy:
    category:
        - Event
---

