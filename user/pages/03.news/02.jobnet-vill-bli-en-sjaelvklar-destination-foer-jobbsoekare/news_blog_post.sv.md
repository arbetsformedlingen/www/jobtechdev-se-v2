---
title: 'Jobnet vill bli en självklar destination för jobbsökare'
custom:
    title: 'Jobnet vill bli en självklar destination för jobbsökare'
    short: 'På hemsidan läser vi: ”Hos Jobnet söker du på dina villkor, anonymt och smidigt.” Vi blev nyfikna och bad om en intervju med Kalle Bodestig, VD och grundare av plattformen Jobnet.'
    content: "\n![Kalle Bodestig](./kalle-kontor-liten.jpg)\n_Kalle Bodestig, VD och grundare av Jobnet_\n\n**Berätta om Jobnet! Vad gör ni?**\n\n”Jobnet är en plattform med ambitionen att bli ett Hemnet för jobbmarknaden; en plats där nyfikna jobbsökare kan leta efter sin nästa arbetsplats utan att behöva registrera sin profil. Vi tror på idén att det är jobbsökaren som väljer sin nästa arbetsplats och inte tvärtom. Företagen som ligger på plattformen får utrymme för ”employer branding” och kan locka kandidater genom att visa upp sina företag. Vi får därmed en bättre matchning med kandidater som har en god insikt i vad den kommande arbetsplatsen erbjuder.”\n\n**Vilka målgrupper vänder ni er till?**\n\n”Huvudfokus är på jobbsökaren. Om vi lyckas locka många jobbsökare till vår plattform så kommer även rekryterare och företag i kölvattnet, eftersom de vill vara där talangerna finns.”\n \n**Vilka JobTech-API:er använder ni på plattfomen Jobnet?**\n\n”[Jobstream](https://www.jobtechdev.se/sv/komponenter/jobstream), [JobAd Enrichments](https://www.jobtechdev.se/sv/komponenter/jobad-enrichments) och [JobTech Taxonomy ](https://www.jobtechdev.se/sv/komponenter/jobtech-taxonomy)är de vi använder oss av i dag.”\n \n**Kan du beskriva hur ni arbetar och hur API:erna har hjälpt er i er verksamhet för att nå bättre resultat? **\n\n”Vi använder API:erna på flera olika sätt. Vår sökstruktur och kategorisering är ett direkt arv från den data som lästs in – ett arbete som annars skulle tagit flera hundra timmar.\nVårt senaste tillämpningsområde är att vi byggt publika statistiksidor som alla kan se. Där kombinerar vi olika datakällor och kan bland annat visa prognoser för olika yrkesroller och kompetenser. Dessa har snabbt blivit populära men hade varit omöjliga för oss att skapa utan dessa API:er.”\n\n**Vad är nästa steg för Jobnet och var är ni om fem år?**\n\n”Nu öppnar vi strax upp vår plattform för externa rekryteringsbyråer, så att de kan lyfta in sina företag. Det gör att vi kan skala vår verksamhet på riktigt. Vårt långsiktiga mål är att bli den största destinationen för jobbsökare efter Arbetsförmedlingen och LinkedIn. Vi har i lugn och ro kunnat utveckla moderna verktyg under ett par år. Nu ska det bli riktigt roligt att ge fler rekryterare möjligheten att använda dem!” \n\n[Besök Jobnet](https://jobnet.se/)\n\n\n\n"
    ingress: "På hemsidan läser vi:\n”Hos Jobnet söker du på dina villkor, anonymt och smidigt.” \n\nVi blev nyfikna och bad om en intervju med Kalle Bodestig, VD och grundare av plattformen Jobnet."
    minititle: Jobnet
date: '2023-05-02 15:56'
taxonomy:
    category:
        - Nyhet
    type:
        - Intervju
media_order: kalle-kontor-liten.jpg
---

