---
title: Aktuellt
routes:
    default: /nyheter
admin:
    children_display_order: collection
twig_first: false
custom:
    text: 'Ta del av aktuella nyheter, artiklar, insikter, kommande event och webinarier, som JobTech Development arrangerar, deltar i, eller är av intresse för det digitala ekosystemet. '
    title: Aktuellt
---

