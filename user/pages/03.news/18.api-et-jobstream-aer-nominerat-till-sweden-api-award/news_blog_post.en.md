---
title: 'JobStream Won Sweden API Award '
custom:
    title: 'The API JobStream Won Sweden API Award '
    date: '2021-11-22 16:04'
    short: 'A dedicated team at JobTech Development has caught up and mapped the users'' needs by developing this API. '
    content: "**The API JobStream won Sweden API Award. A dedicated team at JobTech Development has caught up and mapped the users' needs by developing this API. **\n \nThe nomination underlines: _“The Swedish Public Employment Service revolutionizes the labour market’s matching. JobStream is applicable and useful for companies and originations, which have their own search engine and would like to improve its services with real-time updated job advertisements data. The API could be also used to build and train machine learning’s algorithms. Further, it could be used to identify and analyze labor market trends, based on job ad data.”_\n \n\n**How did the idea come out? \n**\n_“After catching up the users’ needs, one came up with the idea that the real added value lies in the data. As a result, the API has been built, which provides Sweden with the job ads, which already exist. It is as easy as it is to click on a weblink to load and view all job ads at the Job board (in Swedish: Platsbanken). Today, we receive approximately 25 million requests from external partners per month in relation to job advertisements.  The final effect of providing job ads openly is that it will facilitate the process between employers and jobseekers to find each other easily in new ways.”,_ says Jonas Södergren from JobTech Development.\n\nWe go from ego to eko and we are delighted to share what we have and have learnt. The API is free of charge and available to anyone. If you would like to copy the developers’ success story, just follow these steps:_ \"Listen to the user, do one thing and do it well\"._ \n\nLink to the [API.](https://jobtechdev.se/en/components/jobstream)\n"
date: '2021-11-22 16:04'
taxonomy:
    category:
        - News
---

