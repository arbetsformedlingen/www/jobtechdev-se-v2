---
title: 'Inrupt Solid Hackathon 10-28 april'
custom:
    title: 'Inrupt Solid Hackathon 10-28 april'
    date: '2023-04-10 09:00'
    endtime: '2023-04-28 17:00'
    short: 'Hackathon ger utvecklare möjligheten att prova Inrupts nya företagsklientbibliotek för Java-apputveckling, men deltagare är även välkomna att använda våra JavaScript- eller Java-bibliotek för att skapa sitt projekt. Först- och andraplatsvinnare är berättigade till monetära priser och möjlighet att presentera sina projekt på en kommande Solid World.'
    content: "Som en del av Inrupts engagemang för att stärka utvecklare över globen att bygga nya fantastiska upplevelser, är vi glada att presentera vår andra hackathon. Om du är intresserad av att bygga nya Solid-upplevelser för dina användare kan du kickstarta din nya app-idé genom att delta i Inrupt hackathon tillsammans med andra utvecklare.\n\nHackathon ger utvecklare möjligheten att prova Inrupts nya företagsklientbibliotek för Java-apputveckling men deltagare är även välkomna att använda våra JavaScript- eller Java-bibliotek för att skapa sitt projekt.\n\nFörst- och andraplatsvinnare är berättigade till monetära priser och möjlighet att presentera sina projekt på en kommande Solid World.\n\n[Mer info och anmälan (eng)](https://www.inrupt.com/event/solid-hackathon/home\n)\n"
    minititle: '10-28 April'
date: '2023-03-28 14:01'
taxonomy:
    category:
        - Event
---

