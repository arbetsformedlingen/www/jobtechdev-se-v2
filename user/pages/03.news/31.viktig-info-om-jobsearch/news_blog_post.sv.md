---
title: 'Viktig info om JobSearch'
custom:
    title: 'Viktig information om API:et JobSearch'
    short: 'Under september kommer endpointen Taxonomy/Search tas bort.'
    content: "Under september kommer endpointen Taxonomy/Search tas bort. För att kunna göra sökningar i taxonomin hänvisar vi till API:et [JobTech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy), där även möjlighet att använda GraphQL finns.\n\nOm det finns frågor kring detta, ställ dem gärna i [forumet](https://forum.jobtechdev.se/c/vara-api-er-dataset/job-search/28). "
taxonomy:
    category:
        - Nyhet
    type:
        - Information
date: '2022-09-09 11:11'
---

