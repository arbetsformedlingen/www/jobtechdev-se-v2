---
title: 'Foss-north 2023'
custom:
    title: 'JobTech Development at Foss-north 2023'
    date: '2023-04-24 09:00'
    endtime: '2023-04-25 16:00'
    short: 'This year’s event will be covering political, legal and social challenges related to software freedom  as well presenting best practices from Scandinavia. '
    content: "**Foss-north is a free and open-source conference covering broad topics for both software and hardware from the technical perspective.The conference will be held on April 24th-25th, 2023 at Chalmers Conference Centre, Gothenburg, Sweden, paired with the community day on April 23rd. The event will be run on-site. **\n\nThe event brings together Nordic Free Software communities to share knowledge and exchange ideas related to free software. This year’s event will be covering political, legal and social challenges related to software freedom  as well presenting best practices from Scandinavia.\n\nJonas Södergren, Technical Lead and Programmer at JobTech Development, will be among the strong line-up speakers at Foss-north Conference 2023. He will be talking about: **\"Arbetsförmedlingen's experiences with JobTech and open source: Sharing 400 repositories and successful collaborations\"** at 11:00-12:00 CET on April 25th, 2023. \n\nDuring his presentation, Jonas will share insights from successful initiatives, challenges, and collaborations with other organizations within the jobtech space and open-source communities. By sharing practical examples, the audience is expected to gain a better understanding on how open-source can be used in public agencies to be more open and efficient. \n\n\n[For more details and registration](https://foss-north.se/2023/index.html)"
date: '2023-04-12 13:25'
taxonomy:
    category:
        - Event
---

