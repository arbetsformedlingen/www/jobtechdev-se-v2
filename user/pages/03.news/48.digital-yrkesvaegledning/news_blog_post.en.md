---
title: 'Digital Vocational Career Guidance'
custom:
    title: 'Digital Vocational Career Guidance: New Project Fostering Skills Development'
    short: 'JobTech Development is leading an exciting new project within the JobEdTech area, which is based on using enriched CV information to assess which education programs can fill the gap between the individual''s experiences and the labour market''s needs.'
    content: "**JobTech Development is leading an exciting new project within the JobEdTech area, which is based on using enriched CV information to assess which education programs can fill the gap between the individual's experiences and the labour market's needs.**\n\nA career transition in mid-life can be a challenge for many people. Nevertheless, it is just what the labour market will demand more and more from people in the future. We already acknowledge that lifelong learning is necessary to manage and boost the skills supply in Sweden.\n\nThe goal of the project \"Digital Vocational Career Guidance\" is to digitally guide the individual to find the shortest path to a job or career transition through fostering skills development.\n\nThe project is a collaboration between the Swedish Public Employment Service, RISE Research Institutes of Sweden, the Swedish National Agency for Higher Vocational Education and the Swedish National Agency for Education.\n\n[Read more about the project. ](https://jobtechdev.se/en/components/digital-yrkesvaegledning)\n"
date: '2023-03-30 14:58'
taxonomy:
    category:
        - News
---

