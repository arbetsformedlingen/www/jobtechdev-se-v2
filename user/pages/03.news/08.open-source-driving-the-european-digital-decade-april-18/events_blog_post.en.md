---
title: 'Open Source Driving Digital Decade, April 18th'
custom:
    title: 'JobTechDev at Open Source Driving Digital Decade, April 18th'
    date: '2023-04-18 10:00'
    endtime: '2023-04-18 17:30'
    short: 'The event brings together European actors in Stockholm to continue the discussion of the role of open technologies in shaping our shared digital future. '
    content: "**The one-day conference \"Open Source Driving Digital Decade\" is taking place in Stockholm on April 18th, 2023, following the events in Toulouse, France, and Brno, Czech Republic. The organizers OpenForum Europe, the Swedish Internet Foundation, NOSAD (Network Open Source and Data), Open Source Sweden, Red Hat, and the Foundation for Public Code are now bringing together European actors in the Nordic capital to continue the discussion of the role of open technologies in shaping our shared digital future.**\n\n\nMaria Dalhage from JobTech Development, who is as well [NOSAD](https://nosad.se/) Project Manager will participate in the panel **\"Open Source Communities and Enablement: What is needed for public administrations and private actors to collaborate on joint open source projects?\"**\n\nThe panel will discuss the need for new skills, Open Source Program Office (OSPO) and other types of networks and communities to create conditions for a growing collaboration.\n\n**Panelists:**\n* Maria Dalhage, Project Manager Open Data and Open Source, Arbetsförmedlingen\n* Gjis Hillenius, Open Source Programme Office, European Commission\n* Jan Ainali, Codebase Steward, Foundation for Public Code\n* Gabriele Columbro, General Manager, Linux Foundation Europe\n\n**Moderator:**\nLeslie Hawthorne, Senior Manager, Open Source Program Office, Red Hat\n\n**Venue:** Internetstiftelsen, Hammarby kaj 10D, Stockholm, Sverige.\n\n[More details and Registration](https://openforumeurope.org/event/oss-swedish-eu-presidency/)"
date: '2023-03-28 13:25'
taxonomy:
    category:
        - Event
---

