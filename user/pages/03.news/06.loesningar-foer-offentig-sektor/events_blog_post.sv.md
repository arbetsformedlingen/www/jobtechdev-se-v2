---
title: 'Lösningar för offentlig sektor, Kistamässan'
custom:
    date: '2023-01-25 09:00'
    endtime: '2023-01-26 16:30'
    short: 'Välkommen att delta på Sveriges största och mest heltäckande mötesplats för offentliga sektor'
    content: "###### Välkommen att delta på Sveriges största och mest heltäckande mötesplats för offentliga sektor\nHär får du chans till mängder av möten, nya insikter, kompetensutveckling, underlag till upphandlingar, mingel och utbyte av erfarenheter. \nMed över 250 utställare och 200 talare besöker du en mässa, konferens och mötesplats med extra allt – den största i sitt slag.\n\n[För mer info](https://losningarforoffentligsektor.se/)"
    title: 'Lösningar för offentlig sektor, Kistamässan'
date: '2022-11-28 10:05'
taxonomy:
    category:
        - Event
    status:
        - Avslutad
unpublish_date: '2023-02-05 08:25'
---

