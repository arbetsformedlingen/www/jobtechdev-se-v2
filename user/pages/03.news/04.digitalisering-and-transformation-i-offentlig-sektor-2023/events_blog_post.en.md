---
title: 'Digitalisation & transformation in the public sector 2023'
custom:
    title: 'Digitalisation & Transformation in the Public Sector 2023'
    date: '2023-09-20 08:30'
    endtime: '2023-09-20 16:55'
    short: 'This year''s conference will focus on digital strategies, organizational development, and change management. Jonas Södergren and Maria Dalhage from JobTechDev will be sharing insights about customer-focused digitalisation of the Swedish Public Employment Service for a better customer experience and increased efficiency”. '
    content: "**This year's conference on digitalisation and transformation in the public sector will focus on digital strategies, organizational development, and change management.**\n\nDuring the event, you'll have the opportunity to discover successful digital strategies and transformation processes that can help drive the transformation forward.\n\nOne of the case studies will feature JobTech Development's Jonas Södergren and Maria Dalhage, who will be speaking about **“Customer-focused digitalisation of the Swedish Public Employment Service: For a Better Customer Experience and Increased Efficiency”. **\n\n**Other topics, which will be covered during the event: **\n* implementing new processes and work methods to meet increased demand for services and enhance the customer experience\n* leveraging smart technology to save time and improve the efficiency of services for job seekers and employers\n* accelerating digital transformation in the public sector through knowledge sharing\n\n\n[More details in Swedish and enrollment](https://www.abilitypartner.se/konferenser_kurser/digitalisering-och-transformation-i-offentlig-sektor/?utm_source=Epostservice&utm_medium=email&utm_term=&utm_content=&utm_campaign=OFF1733+MM\n)"
taxonomy:
    category:
        - Event
sitemap:
    ignore: false
date: '2023-05-09 09:48'
---

