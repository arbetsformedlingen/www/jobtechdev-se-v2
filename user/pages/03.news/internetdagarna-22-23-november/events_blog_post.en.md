---
title: 'Internet Days Conference 2021'
custom:
    title: 'Internet Days Conference 2021, November 22nd-23rd'
    short: 'Greg Golding from JobTech will be talking about tomorrow''s work and education systems with a focus on skills matching.'
    description: ' '
    date: '2021-11-23 08:00'
    endtime: '2021-11-23 17:00'
    content: "If you have not signed up for the Internet Days, it's time to do it now. \n\nA lot of exciting topics under the theme - \"Broaden your digital perspective\". The event is digital and it takes place between November, 22nd-23rd 2021. \n\nJobTech's unit manager Greg Golding will be speaking under the panel \"Future working life: hybrids, gigs and remote\" on November, 22nd at 10:25-10:40. Greg will be taking a deep dive into tomorrow's work and education systems with a focus on skills matching. \n\nHope to see you there."
taxonomy:
    category:
        - Event
    status:
        - Completed
metadata:
    'Internet Days 2021': ''
---

