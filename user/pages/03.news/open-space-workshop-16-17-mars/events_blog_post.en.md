---
title: 'Open Space Workshop Vol.2'
published: true
custom:
    title: 'Open Space Workshop on March 16th-17th, 2022'
    date: '2022-03-16 10:30'
    endtime: '2022-03-17 15:00'
    short: 'JobTech together with Sunet continues the series of workshops in the format "Open Space" on digitization, skills supply and lifelong learning.'
    content: "###### Welcome to Open Space Vol 2 on digitization, skills supply and lifelong learning\n\nThe event will take place on March, 16th-17th, 2022 at the Internet Foundation's premises, Hammarby Kaj 10D, Stockholm (ie physically on site).\n\nWe welcome everyone in both the public and private sectors to participate and contribute with insights, suggestions and thoughts.\n\n[Here you will find information about the event and registration.](https://www.eventbrite.se/e/open-space-2-digitalisering-kompetensforsorjning-och-livslangt-larande-registrering-268852453927)\n\nWe have only 75 seats available, so hurry up - it's first come, first served. The last day for registration is Monday, March 7th, 2022\n\n"
taxonomy:
    category:
        - Event
    status:
        - Completed
---

