---
title: 'News & Events'
custom:
    title: 'News & Events'
    text: 'You can read about current news, articles, insights, upcoming events and webinars, which JobTech Development is arranging, participating in, or in general are interesting for the digital ecosystem. '
---

