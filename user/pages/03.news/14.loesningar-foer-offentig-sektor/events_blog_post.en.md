---
title: '2023 Public Sector Innovations Conference '
custom:
    date: '2023-01-25 09:00'
    endtime: '2023-01-26 16:30'
    short: 'Welcome to 2023 Public Sector Innovations Conference at Kistamässan. The conference is Sweden’s largest meeting place for everybody who leads, develops, and streamlines public services. '
    content: "**Welcome to 2023 Public Sector Innovations Conference, which is taking place at Kistamässan on January, 25th-26th 2023. The conference is Sweden’s largest meeting place for everybody who leads, develops, and streamlines public services. **\n\nAt the Public Sector Innovations Conference market-leading suppliers connect with leading purchasers, decision makers, officials, elected representatives and influencers from the town and county councils, healthcare authorities and other service providers in the public sector. Over 250 exhibitors, 200 speakers and amazing keynotes speakers. \n\nThe event addresses their most pressing challenges, such as:\n\n* sustainable public procurement to create efficiencies and social value\n* how to achieve cost savings through more collaborative approaches to delivering public services\n* leveraging new capabilities and opportunities through digital transformation\n* attracting, developing, and retaining talent\n* how to plan and build smarter cities and properties\n* innovative procurement and involvement at an early stage\n\n[For more information. ](https://losningarforoffentligsektor.se/engelska/) <BR><BR>\n_\nP.S. Kindly note that most of the events and seminars are run in Swedish. _\n"
    title: '2023 Public Sector Innovations Conference, Kistamässan'
date: '2022-11-28 10:05'
taxonomy:
    category:
        - Event
    status:
        - Completed
unpublish_date: '2023-02-05 08:28'
---

