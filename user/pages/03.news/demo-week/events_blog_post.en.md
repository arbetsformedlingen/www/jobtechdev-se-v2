---
title: 'Demo Week, May 2022'
custom:
    title: 'Demo Week, May 9th-12th, 2022'
    date: '2022-05-09 11:00'
    endtime: '2022-05-12 13:30'
    short: 'We invite warmly everyone to come and join us to learn more about the ongoing projects as part of the governmental assignment on a coherent data infrastructure for lifelong learning and skills supply.'
    content: "**Welcome to Demo Week!**\n\nWe invite warmly everyone to come and join us to learn more about the ongoing projects as part of the [governmental assignment on a coherent data infrastructure for lifelong learning and skills supply](https://www.regeringen.se/regeringsuppdrag/2021/06/uppdrag-att-utveckla-en-sammanhallen-datainfrastruktur-for-kompetensforsorjning-och-livslangt-larande/). \n\n30 minutes long digital presentations of the pilot projects and collaborative initiatives are taking place every day between **May 9th-12th, 2022**. You can learn more among others about: \n\n* Personal data and data portability\n* Education-to job matching and job-to education matching\n* The Swedish National Agency for Education's data hub (Susa hub) \n* Swedish Higher Vocational Education’s qualifications database \n\nEveryone, who is interested, is welcome to join us free of charge. \n\n[Register here](https://www.eventbrite.se/e/demo-week-digitalisering-kompetensforsorjning-och-livslangt-larande-biljetter-328155290307?aff=ebdssbdestsearch&keep_tld=1). \n\nWelcome to share ideas and collaborate in our dedicated community around digitalization, lifelong learning, and the future labour market."
taxonomy:
    category:
        - Event
    status:
        - Completed
metadata:
    'Demoweek, KLL, regeringsuppdraget, JobTech Development': ''
---

