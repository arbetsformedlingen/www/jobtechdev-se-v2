---
title: 'About the website'
custom:
    content: "## This website uses responsive design, and works well in modern browsers that support web standards set by W3C. Modern browsers include Safari, Chrome and Firefox.\n## \n\nOur ambition for the website is to fully meet the criteria and comply with the Web Content Accessibility Guidelines (WCAG 2.1), level AA. It means that we follow the same guidelines for accessibility adaptation as the legal requirements for authorities and other government agencies."
    menu:
        -
            title: Accessibility
            url: /en/about-the-website/availability
        -
            title: 'Cookie policy (in Swedish)'
            url: /en/about-the-website/cookies
    title: 'About the website'
routes:
    default: /about-the-website
---

