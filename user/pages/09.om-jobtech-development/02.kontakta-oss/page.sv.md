---
title: 'Kontakta oss'
custom:
    menu:
        -
            title: 'Samarbeta med oss'
            url: /sv/om-jobtech-development/kom-igang
        -
            title: 'Inspireras av andra'
            url: /sv/om-jobtech-development/inspireras-av-andra
        -
            title: 'Kontakta oss'
            url: /sv/om-jobtech-development/kontakta-oss
    content: "## Kul att du vill komma i kontakt med oss. Vi försöker att vara så snabba som möjligt med att svara på alla slags frågor, funderingar eller feedback.## \n\nVill du ha hjälp eller support relaterat till våra produkter så rekommenderar vi att du använder vårt [community](https://forum.jobtechdev.se) så svarar någon av våra utvecklare eller användare så fort som möjligt. Om du inte vill det tar vi även emot frågor [via mail](mailto:jobtechdev@arbetsformedlingen.se).\n \nÄr du intresserad av att jobba hos oss så lägger vi upp alla våra annonser antingen [här på siten](https://jobtechdev.se/sv/jobba-hos-oss), eller på Arbetsförmedlingens annonsplattform [Platsbanken](https://arbetsformedlingen.se/platsbanken). Vi tar även emot spontanansökningar [via mail](mailto:jobtechrecruitment@arbetsformedlingen.se ).\n  \nHar du frågor kring samarbete eller vill bidra med något till ekosystemet kan du alltid maila [community-teamet](mailto:jobtechdev@arbetsformedlingen.se) så hjälper vi till.\n\nVill du träffa oss så sitter de flesta utspridda i Sverige, men vi har också kontorslokaler i centrala [Stockholm](https://www.google.com/maps/place/Slottsbacken+4,+111+30+Stockholm/@59.3258895,18.0706078,17z/data=!3m1!4b1!4m5!3m4!1s0x465f9d587818d6bb:0x6a3563e7fe09a3bd!8m2!3d59.3258895!4d18.0727965) och Visby där vi kan boka in ett möte.  \n\n<BR><BR><BR><BR>"
    title: Kontakt
---

