/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./themes/jobtechTailwind/templates/**/*.{html,js,twig}","./themes/jobtechTailwind/templates/partials/**/*.{html,js,twig}"],
  theme: {

    
        
    extend: {
      fontFamily: {
       playfair: "playfair",
       roboto_slab: "roboto_slab",
       roboto_reg: "roboto_reg",
   
       },

       fontSize: {
        '75/75':['75px', '75px'],
        '55/65': ['55px', '65px'],
        '15/15' :['15px', '15px'],
        '18/26': ['18px', '26px'],
        '20/30': ['20px', '30px'],
        '20/24': ['20px', '24px'],
        '22/24': ['22px', '24px'],
        '22/35': ['22px', '35px'],
        '30/40': ['30px', '40px'],
        '40/45': ['40px', '45px'],
        '40/60': ['40px', '60px'],
       },
      
       
      colors: {
        'jtd-pink':'#e3ccce',
        '#eee': '#eee',
        '#ddd': '#ddd'
        
      },

      spacing: {    
        '1px': '1px',   
        '8px': '8px',
        '30px':'30px',
        '25px' : '40px',
        '60px': '60px',
        '640px':'640px',
        '40px' : '40px',
        '80px': '80px',
        '750px' : '750px',
        '870px' : '870px',
        '1084px' : '1084px',
        '1144px': '1144px',
        
      
        },
      
        screens: {
          '3xl': '1600px',
          '4xl': '1920px'
        }
        
    },
  },
  plugins: [],
}

