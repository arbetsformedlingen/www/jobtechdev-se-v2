FROM docker.io/library/php:8.3.8-apache-bookworm

ENV GRAV_VER=1.7.42 \
    PHPRC=

# Setup OS and Apache
RUN sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf \
    && sed -i 's/80/8080/' /etc/apache2/ports.conf \
    && sed -i 's/80/8080/' /etc/apache2/sites-enabled/000-default.conf
RUN apt-get update -y \
    &&  apt-get install -y \
        wget \
	    unzip \
	    cron \
        libzip-dev \
        zip \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    &&  apt-get clean
RUN docker-php-ext-install zip \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \
    && a2enmod rewrite
COPY php.ini /usr/local/etc/php/


WORKDIR /var/www/html

# Install Grav
RUN wget https://getgrav.org/download/core/grav-admin/${GRAV_VER} \
    && unzip ${GRAV_VER} -d /var/www/html/ \
    && mv /var/www/html/grav-admin/* /var/www/html/ \
    && rm -rf /var/www/html/grav-admin /var/www/html/${GRAV_VER} \
    && mv /var/www/html/webserver-configs/htaccess.txt /var/www/html/.htaccess \
    && rm -rf /var/www/html/webserver-configs

# Install Extra Grav plugins
RUN bin/gpm install simplesearch \
    && bin/gpm install breadcrumbs \
    && bin/gpm install tecart-cookie-manager \ 
    && bin/gpm install admin \
    && bin/gpm install email \
    && bin/gpm install error \
    && bin/gpm install form \
    && bin/gpm install flex-objects \
    && bin/gpm install login \
    && bin/gpm install markdown-notices \
    && bin/gpm install problems \
    && bin/gpm install sitemap 


# Install versioncontrolled information
COPY themes/jobtech/ /var/www/html/user/themes/jobtech/
COPY themes/jobtechn/ /var/www/html/user/themes/jobtechn/
COPY themes/jobtechTailwind/ /var/www/html/user/themes/jobtechtailwind/
COPY user/config/ /var/www/html/user/config/
COPY scripts/*.sh /bin/
RUN chmod +x /bin/init-volume.sh \
    && mkdir -p /var/www/html-base/user/ \
    && rm -rf /var/www/html/user/pages/* /var/www/html/user/data/* \
              /var/www/html/user/accounts/* \
    && mkdir -p /var/www/html/sessions
COPY user/pages /var/www/html-base/user/pages
COPY user/data /var/www/html-base/user/data
COPY user/accounts /var/www/html-base/user/accounts

EXPOSE 8080
