





//function removeDuplicates() {
//	let resultArray = [];
//	for(arr of arguments){
//		resultArray = resultArray.concat(arr);
//	}
//	return [...new Set(resultArray)];
//}
//
//function renderFilter(filterArray){
//	avalibleTags = [];
//	list = document.getElementsByClassName("listitem");
//	for(el of list){
//		eltags = el.getAttribute('data-tags').split(',');
//		for(tag of filterArray){
//			if(!eltags.includes(tag)){
//				el.classList.add('hidden');
//			}
//		}
//		avalibleTags = !el.classList.contains('hidden')? removeDuplicates(avalibleTags,eltags): avalibleTags;
//	}
//	avalibleTags = removeDuplicates(avalibleTags);
//	buttons = document.getElementsByClassName('tag-button');
//	for(button of buttons){
//		if(!avalibleTags.includes(button.id)){
//			button.classList.add("unavalible");
//			button.disabled = true;
//		}else{
//			button.classList.remove("unavalible");
//			button.disabled = false;
//		}
//	}
//}
//
//function getParamsArray (){
//	let params =  new URLSearchParams(document.location.search);
//	if(params.has('q')){
//		let qs = params.get('q').split(',').filter(value => value != '');
//		return qs;
//	}
//	return [];
//}
//
//
//
//function filter(e){
//	let filter = getParamsArray();
//	e.classList.toggle("active");
//
//	if(e.classList.contains('active')){
//		filter.push(e.innerHTML);
//	}else{
//		filter = filter.filter(item => item != e.innerHTML);
//	}
//
//	history.replaceState(null ,null, "?q="+filter.join(",") )
//	activateButtons(filter);
//}
//
//
//
//
//function activateButtons (array){
//	list = document.getElementsByClassName("listitem");
//
//	for(el of list){
//		el.classList.remove("hidden");
//	}
//
//	if(array.length > 0){
//		array.forEach(element => {
//			document.getElementById(`${element}`).classList.add("active");
//		});		
//	}
//	renderFilter(array);
//	
//}
//
//(() => {
//	let tagbuttons = document.getElementsByClassName('tag-button');
//
//	tagbuttons.addEventListener('click', activateButtons)
//
//	document.addEventListener('DOMContentLoaded', function() {
//		qs = getParamsArray ();
//		activateButtons(qs);
//	}, false);	
//})()





// let chosenTagsArray = [];
// let avalibleTagsArray = [];
// chosenTagsArray.push = function() {
// 	Array.prototype.push.apply(this, arguments);
// 	tagChosen(arguments);
// };
// chosenTagsArray.remove = function() {
// 	for(tag of arguments){
// 		element = document.getElementById(tag);
// 		element.classList.toggle('active')
// 		element.addEventListener("click", function() {
// 			chosenTagsArray = chosenTagsArray.filter(item => item != tag.id);
// 		})
// 	}
// 	history.replaceState(null ,null, "?q="+chosenTagsArray.join(",") )
// 	console.log(chosenTagsArray)
// };


// document.addEventListener('DOMContentLoaded', function() {
	
	
// 	let tagbuttons = document.querySelectorAll('.tag-button');
// 	tagbuttons.forEach(function(element) {
// 		element.addEventListener("click", function() {
// 			chosenTagsArray.push(this.id);
// 		});
// 	});
// }, false);	

// function tagChosen(args){
// 	for(tag of args){
// 		element = document.getElementById(tag);
// 		element.classList.toggle('active');
// 		element.addEventListener("click", function() {
// 			chosenTagsArray.remove(element.id);
// 		})
// 	}
// 	history.replaceState(null ,null, "?q="+chosenTagsArray.join(",") )
// 	console.log(chosenTagsArray)
	
// }
// function tagUnchosen(args){
	
	
// }

// 
// 
// 
// 
// 
// 
// 


function setLanguage(lang) {
	if(window.location.pathname.length >= 3) {
		var path = window.location.pathname.substring(4).split('/');
		var root = path[0];

		if(lang == "en") {
			if(root == "om-jobtech-development") {
				root = "about-jobtech-development";
			} else if(root == "komponenter") {
				root = "components";
			} else if(root == "nyheter") {
				root = "news";
			} else if(root == "jobba-hos-oss") {
				root = "work-with-us";
			} else if(root == "om-webbplatsen") {
				root = "about-the-website";
			} else if(root == "kontakt") {
				root = "contact-us";
			} else if(root == "kalender") {
				root = "calendar";
			}
		} else {
			if(root == "about-jobtech-development") {
				root = "om-jobtech-development";
			} else if(root == "components") {
				root = "komponenter";
			} else if(root == "news") {
				root = "nyheter";
			} else if(root == "work-with-us") {
				root = "jobba-hos-oss";
			} else if(root == "about-the-website") {
				root = "om-webbplatsen";
			} else if(root == "contact-us") {
				root = "kontakt";
			} else if(root == "calendar") {
				root = "kalender";
			}
		}
		path[0] = root;
		// rebuild uri
		var target = "/" + lang;
		for(var i=0; i<path.length; ++i) {
			target = target + "/" + path[i];
		}
		window.location.pathname = target;
	} else {
		window.location.pathname = "/" + lang;
	}
}

function removeElement(e) {
	if(e && e.parentNode) {
		e.parentNode.removeChild(e);
	}
}

function toggleStyle(element, style) {
	if(element.classList) {
		element.classList.toggle(style);
	} else {
		// IE9
		var classes = element.className.split(" ");
		var i = classes.indexOf(style);
		if (i >= 0) {
			classes.splice(i, 1);
		} else {
			classes.push(style);
		}
		element.className = classes.join(" ");
	}
}

function mobileMenuClose() {
	var menu = document.getElementById("main_menu");
	var menuButton = document.getElementById("main_menu_button_close");
	if(menu.classList) {
		menu.classList.add("hidden_menu");
		menuButton.classList.remove("close");
	} else {
		// IE9
		var classes = menu.className.split(" ");
		var i = classes.indexOf("hidden_menu");
		if (i == -1) {
			classes.push("hidden_menu");
			menu.className = classes.join(" ");
			// change button icon
			menuButton.className = menuButton.className.split(" ")[0];
		}
	}
}

function mobileMenuToggle(element) {
	var menu = document.getElementById("main_menu");
	var menuButton = document.getElementById("main_menu_button");
	toggleStyle(menu, "hidden_menu");
	toggleStyle(menuButton, "close");
}

function mobileTouchHook() {
	document.addEventListener("mousedown", function(e) {
		var menu = document.getElementById("main_menu");
		var menuButton = document.getElementById("main_menu_button");
		if(e.path.indexOf(menu) == -1 && e.path.indexOf(menuButton) == -1) {
			mobileMenuClose();
		}
	});
}

function acceptCookies() {
	localStorage.setItem("jtCookiesAccepted", true);
	removeElement(document.getElementById('cookie_notification'));
}

function statusClicked() {
	sessionStorage.setItem("jtStatusAccepted", Date.now());
	removeElement(document.getElementById('status_notification'));
}

var callback = function() {
	mobileTouchHook();
	// check if we should display cookie notification
	if(localStorage.getItem("jtCookiesAccepted") == null) {
		document.getElementById("cookie_notification").style.display = "flex";
	}
	// check if we should show error message
	if(sessionStorage.getItem("jtStatusAccepted") != null) {
		var elapsed = Date.now() - parseInt(sessionStorage.getItem("jtStatusAccepted"));
		if(elapsed < 60 * 60 * 1000) {
			removeElement(document.getElementById('status_notification'));
		}
	}
	// apply filter 
	var raw = decodeURIComponent(window.location.hash);
	var index = raw.indexOf("f=");
	if(index != -1) {
		raw = raw.substring(index + 2);
		if(raw.indexOf('&') != -1) {
			raw = raw.substring(0, raw.indexOf('&'));
		}
		var element = document.getElementById("tag_" + raw);
		
		if(element != null) {
			toggleFilter(element, raw);
		}
	}
};









